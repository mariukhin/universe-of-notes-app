import { StarTwoTone } from '@ant-design/icons';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

import {
  AuthorLink,
  AuthorName,
  CardPreview,
  CustomCard,
  FavoriteNumBlock,
  NoteLink,
} from '../styledComponents';

const NoteCard = ({
  id,
  title,
  loading,
  previewContent,
  author,
  favoriteNum,
  clickOnLike,
  isLikedBefore,
}) => {
  const [isLike, setIsLike] = useState(isLikedBefore);
  const [cardRating, setCardRating] = useState(favoriteNum);

  return loading ? (
    <CustomCard loading />
  ) : (
    <CardPreview
      hoverable
      title={title}
      extra={<NoteLink to={`/app/note-details/${id}`}>More</NoteLink>}
      actions={[
        <FavoriteNumBlock
          key="favorite"
          isliked={isLike ? 1 : 0}
          onClick={() => {
            clickOnLike(id, !isLike);
            setIsLike(!isLike);
            setCardRating(!isLike ? cardRating + 1 : cardRating - 1);
          }}
        >
          <StarTwoTone twoToneColor="var(--clam-shell)" />
          <p>{cardRating}</p>
        </FavoriteNumBlock>,
        <AuthorName key="author">
          <AuthorLink to={`/app/user-details/${author}`}>{author}</AuthorLink>
        </AuthorName>,
      ]}
    >
      <p style={{ wordWrap: 'break-word' }}>{previewContent}</p>
    </CardPreview>
  );
};

NoteCard.propTypes = {
  id: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  previewContent: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  favoriteNum: PropTypes.number.isRequired,
  clickOnLike: PropTypes.func.isRequired,
  isLikedBefore: PropTypes.bool.isRequired,
};

export default NoteCard;
