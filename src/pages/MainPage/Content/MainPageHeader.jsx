import { LogoutOutlined } from '@ant-design/icons';

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import { fetchUserNotes } from 'Actions/notes';
import { logout } from 'Actions/session';
import logo from 'Assets/logo.png';

import { routePaths } from 'Routes';

import { getNotes, getUserNotes } from 'Selectors/notes';
import { getBaseUrl } from 'Services/axiosDefaults';
import { FETCHING_STATE } from 'Services/reduxHelpers';

import { NOTIFICATION_TYPES, openNotificationWithIcon } from 'Utils';

import {
  Header,
  Logo,
  HeaderItemsList,
  HeaderListItemLink,
  HeaderListItem,
  HeaderListItemText,
  HeaderUserItemsList,
  LogoutButton,
  Search,
} from '../styledComponents';

const MainPageHeader = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const notes = useSelector(getNotes);
  const userNotes = useSelector(getUserNotes);

  const [search, setSearch] = useState('');

  const isMainPage = location.pathname === routePaths.main;
  const isNotesFetching =
    notes.state === FETCHING_STATE.FETCHING ||
    userNotes.state === FETCHING_STATE.FETCHING;

  const logoutHandler = () => {
    dispatch(
      logout({
        onError: error => {
          const { data } = error;
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data.data,
          });
        },
      }),
    );
  };

  const searchHandler = () => {
    const payload = {
      name: search,
      page: 1,
    };
    if (search) {
      dispatch(fetchUserNotes(payload));
      setSearch('');
    }
  };

  return (
    <Header>
      <Logo
        src={logo}
        alt="logo"
        onClick={() => history.push(routePaths.main)}
      />
      <HeaderItemsList>
        <HeaderListItem onClick={() => history.push(routePaths.user)}>
          <HeaderListItemText>Notes</HeaderListItemText>
          <span />
        </HeaderListItem>
        <HeaderListItem onClick={() => history.push(routePaths.docs)}>
          <HeaderListItemText>Docs</HeaderListItemText>
          <span />
        </HeaderListItem>
        <HeaderListItem>
          <HeaderListItemLink href={getBaseUrl()} target="_blank">
            APi Docs
          </HeaderListItemLink>
          <span />
        </HeaderListItem>
      </HeaderItemsList>
      <HeaderUserItemsList isMainPage={isMainPage}>
        {isMainPage && (
          <Search
            placeholder="Search notes"
            value={search}
            onChange={({ target: { value } }) => setSearch(value)}
            onSearch={searchHandler}
            disabled={isNotesFetching}
            enterButton
          />
        )}
        <LogoutButton
          title="Logout"
          icon={<LogoutOutlined />}
          onClick={logoutHandler}
        />
      </HeaderUserItemsList>
    </Header>
  );
};

export default MainPageHeader;
