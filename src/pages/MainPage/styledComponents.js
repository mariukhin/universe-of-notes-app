import { Input, Card } from 'antd';

import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { ActionItem } from 'Components';

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 65px;
  background-color: var(--alabaster);
  box-shadow: 0px 7px 25px -2px rgba(16, 41, 71, 0.2);
`;

export const HeaderItemsList = styled.ul`
  width: 25%;
  display: flex;
  justify-content: space-between;
`;

export const HeaderListItem = styled.li`
  display: flex;
  flex-direction: column;

  &:hover span {
    opacity: 1;
  }

  span {
    opacity: 0;
    width: 100%;
    margin-top: 3px;
    height: 5px;
    background-color: var(--soft-amber);
    border: 1px solid var(--blue-zodiac);
    border-radius: 5px;
    transition: opacity 1.6s;
  }
`;

export const HeaderListItemText = styled.p`
  color: var(--blue-zodiac);
  font-family: 'Montserrat';
  cursor: pointer;
  font-size: 16px;
`;

export const HeaderListItemLink = styled.a`
  color: var(--blue-zodiac);
  font-family: 'Montserrat';
  cursor: pointer;
  font-size: 16px;

  &:hover {
    color: var(--blue-zodiac);
  }
`;

export const HeaderUserItemsList = styled.div`
  width: 23%;
  display: flex;
  justify-content: ${({ isMainPage }) =>
    isMainPage ? 'space-between' : 'flex-end'};
`;

export const Search = styled(Input.Search)`
  width: 75%;

  & .ant-btn {
    background-color: var(--blue-zodiac);
    border: none;
  }

  & .ant-btn,
  & .ant-input-group-addon {
    border-radius: 0 10px 10px 0;
  }

  & .ant-input {
    border-radius: 10px 0 0 10px;
  }

  & .ant-btn-primary:hover,
  & .ant-btn-primary:focus {
    background-color: var(--soft-amber) !important;
    color: var(--blue-zodiac) !important;
    border: none !important;
  }

  & .ant-input:hover,
  & .ant-input:focus {
    box-shadow: none;
    border: 1px solid var(--blue-zodiac);
    color: var(--blue-zodiac);
  }
`;

export const Wrapper = styled.main`
  display: grid;
  grid-template-columns: minmax(150px, 70%) 1fr;
  margin-top: 33px;
  background-color: var(--merino);
  height: 100%;
`;

export const MainLeftBlock = styled.div`
  padding: 0px 40px;
`;

export const CardsWrapper = styled.div`
  margin: 0 auto;
  width: 90%;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  align-content: flex-start;
`;

export const CustomCard = styled(Card)`
  width: 40%;
  margin-bottom: 60px;
  border: 1px solid var(--swiss-coffee);
  border-radius: 5px;
  background-color: var(--alabaster);

  &:nth-child(odd) {
    margin-left: 30px;
    margin-right: 60px;
  }
`;

export const CardPreview = styled(CustomCard)`
  .ant-card-head-title {
    text-align: center;
    color: var(--blue-zodiac);
  }

  .ant-card-actions {
    border-radius: 5px;
    background-color: var(--alabaster);

    li > span {
      &:hover {
        color: var(--blue-zodiac);
      }
    }
  }
`;

export const PopularUsersWrapper = styled.div`
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  width: 70%;
`;

export const PopularUsersCard = styled(Card)`
  width: 100%;
  background-color: var(--alabaster);
  text-align: center;

  .ant-card-head-title {
    text-align: center;
    color: var(--blue-zodiac);
  }

  a {
    font-size: 16px;
    color: var(--clam-shell);

    &:hover {
      color: var(--blue-zodiac);
    }
  }

  span {
    line-height: 30px;
    margin-left: 8px;
    margin-right: 8px;
  }

  span:last-child {
    opacity: 0;
  }
`;

export const PopularUsersCardContentBlock = styled.div`
  margin: 0 auto;
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const FavoriteNumBlock = styled.div`
  display: flex;
  width: 20%;
  justify-content: space-between;
  margin: 0 auto;

  .anticon-star {
    svg {
      height: 20px;
      width: 20px;
    }

    svg > path:first-child {
      fill: ${props =>
        props.isliked ? `var(--clam-shell)` : `var(--white)`} !important;
    }
  }

  p {
    margin-left: 6px;
    font-size: 16px;
    line-height: 23px;
    color: ${props => props.isliked && `var(--hurricane)`} !important;
    font-weight: ${props => props.isliked && '500'};
  }
`;

export const AuthorName = styled.p`
  color: var(--blue-zodiac);

  &:hover {
    color: var(--clam-shell);
  }
`;

export const MainRightBlock = styled.div`
  background-color: transparent;
  height: 100%;
  display: flex;
  padding-bottom: 60px;
`;

export const MainDivider = styled.span`
  opacity: 0.4;
  height: 100%;
  width: 7px;
  background-color: var(--alabaster);
  border: 1px solid var(--blue-zodiac);
  border-radius: 5px;
`;

export const Logo = styled.img`
  display: block;
  width: 70px;
  height: 70px;
  cursor: pointer;
`;

export const LogoutButton = styled(ActionItem)`
  &:hover {
    background-color: var(--soft-amber) !important;
    border: 1px solid var(--soft-amber) !important;
    color: var(--blue-zodiac) !important;
  }
`;

export const AuthorLink = styled(Link)`
  &:hover {
    color: var(--blue-zodiac) !important;
  }
`;

export const NoteLink = styled(Link)`
  color: var(--clam-shell);

  &:hover {
    color: var(--blue-zodiac);
  }
`;
