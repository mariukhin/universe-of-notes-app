import { Skeleton } from 'antd';

import React from 'react';
import styled from 'styled-components';

const ToolbarSkeleton = () => (
  <ToolbarSkeleton.Skeleton>
    {new Array(10).fill(0).map((item, idx) => {
      const key = item + idx;
      return <Skeleton.Avatar key={key} size="small" shape="square" />;
    })}
    <Skeleton.Input style={{ width: 120 }} size="small" />
    {new Array(6).fill(0).map((item, idx) => {
      const key = item + idx;
      return <Skeleton.Avatar key={key} size="small" shape="square" />;
    })}
  </ToolbarSkeleton.Skeleton>
);

ToolbarSkeleton.Skeleton = styled.div`
  display: flex;

  & .ant-skeleton-element {
    margin-right: 10px;
  }
`;

export default ToolbarSkeleton;
