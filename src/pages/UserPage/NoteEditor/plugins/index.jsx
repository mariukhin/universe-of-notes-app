import alignmentPlugin, { AlignmentTool } from './alignment';
import counterPlugin, {
  CharCounter,
  LineCounter,
  WordCounter,
} from './counter';
import dndPlugin from './dnd';
import emojiPlugin, { EmojiSelect, EmojiSuggestions } from './emoji';
import focusPlugin from './focus';
import hashtagPlugin from './hashtag';
import imagePlugin from './image';
import linkifyPlugin from './linkify';
import resizablePlugin from './resizable';
import undoPlugin, { RedoButton, UndoButton } from './undo';
import videoPlugin, { addVideo } from './video';

const plugins = [
  alignmentPlugin,
  counterPlugin,
  dndPlugin,
  emojiPlugin,
  focusPlugin,
  hashtagPlugin,
  imagePlugin,
  linkifyPlugin,
  resizablePlugin,
  undoPlugin,
  videoPlugin,
];

export {
  addVideo,
  AlignmentTool,
  CharCounter,
  EmojiSelect,
  EmojiSuggestions,
  LineCounter,
  RedoButton,
  UndoButton,
  WordCounter,
};

export default plugins;
