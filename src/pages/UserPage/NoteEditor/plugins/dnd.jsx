import createBlockDndPlugin from 'draft-js-drag-n-drop-plugin';

const blockDndPlugin = createBlockDndPlugin();

export default blockDndPlugin;
