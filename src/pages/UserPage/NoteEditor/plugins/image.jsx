import 'draft-js-image-plugin/lib/plugin.css';

import createImagePlugin from 'draft-js-image-plugin';
import { composeDecorators } from 'draft-js-plugins-editor';

import alignmentPlugin from './alignment';
import dndPlugin from './dnd';
import focusPlugin from './focus';
import resizablePlugin from './resizable';

const decorator = composeDecorators(
  alignmentPlugin.decorator,
  dndPlugin.decorator,
  focusPlugin.decorator,
  resizablePlugin.decorator,
);

const imagePlugin = createImagePlugin({ decorator });

export default imagePlugin;
