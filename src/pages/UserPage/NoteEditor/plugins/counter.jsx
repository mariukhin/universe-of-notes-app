import 'draft-js-counter-plugin/lib/plugin.css';

import createCounterPlugin from 'draft-js-counter-plugin';

const counterPlugin = createCounterPlugin();

export const { CharCounter, LineCounter, WordCounter } = counterPlugin;

export default counterPlugin;
