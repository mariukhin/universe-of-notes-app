import { Button } from 'antd';

import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { fetchFolder } from 'Api/folders';

import { getSelectedNote } from 'Selectors/notes';

const RecentlyDeletedOverlay = () => {
  const [folderName, setFolderName] = useState('');

  const selectedNote = useSelector(getSelectedNote);

  const updateFolderName = async () => {
    const { folderId } = selectedNote;
    const {
      data: { name },
    } = await fetchFolder({ folderId });
    setFolderName(name);
  };

  useEffect(() => {
    updateFolderName();

    return () => {
      setFolderName('');
    };
  }, [selectedNote]);

  const activateNoteHandler = () => {
    const { id } = selectedNote;
    selectedNote.activateNoteHandler(id);
  };

  const deleteNoteHandler = () => {
    const { id } = selectedNote;
    selectedNote.deleteInactiveNote(id);
  };

  return (
    <RecentlyDeletedOverlay.Wrapper>
      <div>
        <RecentlyDeletedOverlay.Folder>
          {folderName ? `Folder name: ${folderName}` : 'Loading...'}
        </RecentlyDeletedOverlay.Folder>
        <RecentlyDeletedOverlay.ActionButton
          style={{ marginRight: 20, backgroundColor: 'var(--blue-zodiac)' }}
          onClick={activateNoteHandler}
        >
          Recover
        </RecentlyDeletedOverlay.ActionButton>
        <RecentlyDeletedOverlay.ActionButton
          style={{ backgroundColor: 'var(--clam-shell)' }}
          onClick={deleteNoteHandler}
        >
          Delete
        </RecentlyDeletedOverlay.ActionButton>
      </div>
    </RecentlyDeletedOverlay.Wrapper>
  );
};

RecentlyDeletedOverlay.ActionButton = styled(Button)`
  width: 90px;
  color: var(--white);
  border-radius: 5px;
  border: 2px solid var(--white);

  &:hover,
  &:focus {
    color: var(--merino);
    border-color: var(--merino);
  }
`;

RecentlyDeletedOverlay.Folder = styled.p`
  text-align: center;
  margin-bottom: 20px;
  font-weight: 300;
`;

RecentlyDeletedOverlay.Wrapper = styled.div`
  background-color: rgba(138, 133, 132, 0.3);
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default RecentlyDeletedOverlay;
