import {
  AlignCenterOutlined,
  AlignLeftOutlined,
  AlignRightOutlined,
  BoldOutlined,
  CodeOutlined,
  ItalicOutlined,
  OrderedListOutlined,
  UnderlineOutlined,
  UnorderedListOutlined,
  StrikethroughOutlined,
} from '@ant-design/icons';

export const styles = ['BOLD', 'ITALIC', 'UNDERLINE', 'CODE', 'STRIKETHROUGH'];

export const BLOCK_TYPES = [
  { label: 'AL', style: 'align-left', icon: AlignLeftOutlined },
  { label: 'AC', style: 'align-center', icon: AlignCenterOutlined },
  { label: 'AR', style: 'align-right', icon: AlignRightOutlined },
  { label: 'UL', style: 'unordered-list-item', icon: UnorderedListOutlined },
  { label: 'OL', style: 'ordered-list-item', icon: OrderedListOutlined },
];

export const BLOCK_TYPE_HEADINGS = [
  { label: 'h1', style: 'header-one' },
  { label: 'h2', style: 'header-two' },
  { label: 'h3', style: 'header-three' },
  { label: 'h4', style: 'header-four' },
  { label: 'h5', style: 'header-five' },
  { label: 'h6', style: 'header-six' },
];

export const getBlockStyle = block => {
  switch (block.getType()) {
    case 'blockquote':
      return 'RichEditor-blockquote';
    case 'align-left':
      return 'blockAlignLeft';
    case 'align-center':
      return 'blockAlignCenter';
    case 'align-right':
      return 'blockAlignRight';
    default:
      return null;
  }
};

export const getIconByStyle = style => {
  switch (style) {
    case 'BOLD':
      return BoldOutlined;
    case 'ITALIC':
      return ItalicOutlined;
    case 'UNDERLINE':
      return UnderlineOutlined;
    case 'CODE':
      return CodeOutlined;
    case 'STRIKETHROUGH':
      return StrikethroughOutlined;
    default:
      return null;
  }
};

export const getBlockTypeHeadingsValue = blockType =>
  BLOCK_TYPE_HEADINGS.map(item => item.style).includes(blockType)
    ? blockType
    : '';

export const getColorsStyleMap = colors =>
  colors.reduce((acc, color) => {
    acc[color] = { color };
    return acc;
  }, {});

export const pickerColors = [
  '#102947',
  '#3272d7',
  '#8a80a3',
  '#d732c1',
  '#3ab428',
  '#d73241',
  '#b43c28',
  '#b49828',
];
