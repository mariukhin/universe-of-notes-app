import PropTypes from 'prop-types';

import { Button, Tooltip } from 'antd';
import { ReadOutlined, SaveOutlined, SyncOutlined } from '@ant-design/icons';

import { AtomicBlockUtils, EditorState, RichUtils } from 'draft-js';
import React from 'react';
import styled from 'styled-components';

import { applyBlur, toBase64 } from 'Utils';
import { ImageUpload } from 'Components';
import { BLOCK_TYPES, getIconByStyle, styles } from './utils';

import AddVideoAction from './AddVideoAction';
import ColorPicker from './ColorPicker';
import HeadingsSelect from './HeadingsSelect';
import ToolbarSkeleton from './ToolbarSkeleton';

const Toolbar = ({
  addVideo,
  editorState,
  readOnly,
  saveNote,
  setEditorState,
  toggleReadOnlyState,
  isSaving,
  locked,
  redoButton: RedoButton,
  undoButton: UndoButton,
  emojiSuggestions: EmojiSuggestions,
  emojiSelect: EmojiSelect,
}) => {
  const onStyleClick = (e, style) => {
    const newState = RichUtils.toggleInlineStyle(editorState, style);
    setEditorState(newState);
    applyBlur(e);
  };

  const toggleBlockType = blockType => {
    const newState = RichUtils.toggleBlockType(editorState, blockType);
    setEditorState(newState);
  };

  const insertImage = (prevState, base64) => {
    const contentState = prevState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'image',
      'IMMUTABLE',
      { src: base64 },
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(prevState, {
      currentContent: contentStateWithEntity,
    });
    return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ');
  };

  const handleUpload = async e => {
    const base64 = await toBase64(e.file);
    const newEditorState = insertImage(editorState, base64);
    setEditorState(newEditorState);
  };

  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();
  const inlineStyle = editorState.getCurrentInlineStyle();

  return (
    <Toolbar.Wrapper>
      <Toolbar.Container>
        {!readOnly ? (
          <>
            {styles.map(style => {
              const Icon = getIconByStyle(style);
              return (
                <Tooltip
                  mouseEnterDelay={1}
                  key={style}
                  placement="topRight"
                  title={style.toLowerCase()}
                >
                  <Toolbar.ActionButton
                    applied={inlineStyle.has(style) ? 'active' : undefined}
                    size="small"
                    icon={<Icon />}
                    onClick={e => onStyleClick(e, style)}
                  />
                </Tooltip>
              );
            })}
            {BLOCK_TYPES.map(type => (
              <Tooltip
                mouseEnterDelay={1}
                key={type.label}
                placement="topRight"
                title={type.style}
              >
                <Toolbar.ActionButton
                  size="small"
                  icon={<type.icon />}
                  applied={blockType === type.style ? 'active' : undefined}
                  onClick={e => {
                    e.preventDefault();
                    toggleBlockType(type.style);
                    applyBlur(e);
                  }}
                />
              </Tooltip>
            ))}
            <HeadingsSelect
              editorState={editorState}
              toggleBlockType={toggleBlockType}
            />
            <Tooltip mouseEnterDelay={1} placement="topRight" title="add image">
              <ImageUpload onChange={handleUpload} />
            </Tooltip>
            <ColorPicker onStyleClick={onStyleClick} />
            <UndoButton onClick={e => applyBlur(e)} />
            <RedoButton onClick={e => applyBlur(e)} />
            <AddVideoAction
              addVideo={addVideo}
              editorState={editorState}
              setEditorState={setEditorState}
            />
            <EmojiSuggestions />
            <EmojiSelect />
          </>
        ) : (
          <ToolbarSkeleton />
        )}
        <Tooltip mouseEnterDelay={1} placement="topRight" title="readonly">
          <Toolbar.ActionButton
            applied={readOnly ? 'active' : undefined}
            size="small"
            disabled={locked}
            icon={<ReadOutlined />}
            onClick={e => {
              toggleReadOnlyState();
              applyBlur(e);
            }}
          />
        </Tooltip>
        <Tooltip mouseEnterDelay={1} placement="topRight" title="save">
          <Toolbar.ActionButton
            size="small"
            disabled={locked || readOnly}
            icon={<SaveOutlined />}
            onClick={e => {
              saveNote();
              applyBlur(e);
            }}
          />
        </Tooltip>
      </Toolbar.Container>
      {isSaving && (
        <SyncOutlined
          spin
          style={{
            fontSize: 16,
            marginRight: 10,
            color: 'var(--blue-zodiac)',
          }}
        />
      )}
    </Toolbar.Wrapper>
  );
};

Toolbar.propTypes = {
  addVideo: PropTypes.func.isRequired,
  editorState: PropTypes.object.isRequired,
  readOnly: PropTypes.bool.isRequired,
  redoButton: PropTypes.func.isRequired,
  saveNote: PropTypes.func.isRequired,
  setEditorState: PropTypes.func.isRequired,
  toggleReadOnlyState: PropTypes.func.isRequired,
  undoButton: PropTypes.func.isRequired,
  emojiSuggestions: PropTypes.func.isRequired,
  emojiSelect: PropTypes.func.isRequired,
  isSaving: PropTypes.bool.isRequired,
  locked: PropTypes.bool.isRequired,
};

Toolbar.ActionButton = styled(Button)`
  color: ${({ applied }) => (applied ? 'var(--black)' : 'var(--blue-zodiac)')};
  border: ${({ applied }) =>
    applied ? '1px solid var(--black)' : '1px solid var(--blue-zodiac)'};
  margin-right: 10px;
  background-color: ${({ applied }) => (applied ? 'var(--merino)' : 'inherit')};
`;

Toolbar.ChangeMessage = styled.span`
  font-weight: 500;
  color: var(--hurricane);
`;

Toolbar.Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

Toolbar.Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;

  & .ant-btn:hover,
  & .ant-btn:focus {
    background-color: var(--merino);
    color: var(--black);
    border: 1px solid var(--black);
  }

  & .draftJsEmojiPlugin__emojiSelectButton__3sPol,
  & .draftJsEmojiPlugin__emojiSelectButtonPressed__2Tezu {
    border: none;
    display: flex;
    width: 100%;
    height: 100%;
    background-color: inherit;
  }

  & .draftJsEmojiPlugin__emojiSelectButton__3sPol:hover,
  & .draftJsEmojiPlugin__emojiSelectButtonPressed__2Tezu:hover {
    background-color: inherit;
  }

  & .draftJsEmojiPlugin__emojiSelectPopover__1J1s0 {
    right: 10px;
  }
`;

export default Toolbar;
