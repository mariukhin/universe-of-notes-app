import { Alert, Dropdown, Input, Menu, Spin } from 'antd';
import { LockOutlined, SettingFilled, UnlockOutlined } from '@ant-design/icons';

import * as R from 'ramda';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import {
  resetFolderDeletableState,
  setFolderDeletableState,
} from 'Actions/folders';
import {
  activateNote,
  createNote,
  deactivateNote,
  deleteNotes,
  disableNotes,
  editNote,
  enableNotes,
  fetchNotes,
  fetchRecentlyDeletedNotes,
  filterNotes,
  resetNoteState,
  selectNote,
} from 'Actions/notes';
import { fetchRecentlyDeletedNotes as recentlyDeletedNotesApi } from 'Api/notes';
import { getSelectedFolder } from 'Selectors/folders';
import { getNotes, getSelectedNote, isNotesDisabled } from 'Selectors/notes';
import { getUser } from 'Selectors/session';

import {
  getItemByKey,
  getMenuItemId,
  getMenuItems,
  getMenuItemsMaxKey,
  getNewMenuItemName,
  getNotePreviewText,
  getNoteTime,
  NOTIFICATION_TYPES,
  openNotificationWithIcon,
} from 'Utils';

import { confirm, listContextMenu } from 'Components';
import { FETCHING_STATE } from 'Services/reduxHelpers';

import {
  LoadingOverlay,
  MenuStatus,
  Search,
  SearchContainer,
} from './styledComponents';

const NotesList = () => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);

  const [defaultNoteName, setDefaultNoteName] = useState('');
  const [noteName, setNoteName] = useState('');
  const [menuItems, setMenuItems] = useState([]);
  const [search, setSearch] = useState('');
  const [selectedKey, setSelectedKey] = useState('');
  const [isLocking, setIsLocking] = useState(false);

  const notes = useSelector(getNotes);
  const selectedFolder = useSelector(getSelectedFolder);
  const selectedNote = useSelector(getSelectedNote);
  const user = useSelector(getUser);
  const isDisabled = useSelector(isNotesDisabled);

  useEffect(() => {
    const folderId = selectedFolder.id;
    if (selectedFolder.name === 'Recently Deleted') {
      dispatch(fetchRecentlyDeletedNotes());
    } else if (folderId) {
      dispatch(fetchNotes({ folderId }));
      setSearch('');
    }
  }, [selectedFolder.id]);

  useEffect(() => {
    const newItems = getMenuItems(notes.data);
    setMenuItems(newItems);
    const firstItem = newItems[0];
    if (firstItem) {
      setSelectedKey(firstItem.key);
    }
  }, [notes.data]);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
      inputRef.current.select();
    }
  }, [menuItems]);

  const createNewNoteHandler = async () => {
    const folderId = selectedFolder.id;
    if (!folderId) return;

    const newItemProps = {
      key: String(menuItems.length > 0 ? getMenuItemsMaxKey(menuItems) + 1 : 1),
      edit: true,
    };

    dispatch(disableNotes());
    const recentlyDeleted = await recentlyDeletedNotesApi();
    const recentlyDeletedFromFolder = recentlyDeleted.data.filter(
      item => item.folderId === folderId,
    );
    const recentlyDeletedMenuItems = getMenuItems(recentlyDeletedFromFolder);

    dispatch(
      createNote({
        payload: {
          name: getNewMenuItemName(
            menuItems.concat(recentlyDeletedMenuItems),
            'Note',
          ),
          folderId,
          userId: user.id,
        },
        onSuccess: ({ data }) => {
          setSelectedKey(newItemProps.key);
          setNoteName(data.name);
          setDefaultNoteName(data.name);
          const newItem = {
            ...data,
            ...newItemProps,
          };
          setMenuItems(prev => [newItem, ...prev]);
          dispatch(enableNotes());
        },
        onError: () => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: 'Create note error',
          });
          dispatch(enableNotes());
        },
      }),
    );
  };

  const setFilteredMenuItems = id => {
    setMenuItems(prev => {
      let index = 0;
      const filteredItems = prev.filter((item, idx) => {
        const condition = item.id !== id;
        if (!condition) index = idx;
        return condition;
      });
      if (filteredItems.length !== 0) {
        const item = filteredItems[index];
        if (item) {
          setSelectedKey(item.key);
          const suffix = '-notes-li';
          const targetId = id + suffix;
          const targetEl = document.getElementById(targetId);
          const nextEl = targetEl && targetEl.nextSibling;
          if (nextEl) {
            nextEl.focus();
          }
        }
      }
      return filteredItems;
    });
  };

  const activateNoteHandler = id => {
    dispatch(disableNotes());
    dispatch(
      activateNote({
        noteId: id,
        onSuccess: () => {
          setFilteredMenuItems(id);
          dispatch(enableNotes());
        },
        onError: () => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: 'Activate note error',
          });
          dispatch(enableNotes());
        },
      }),
    );
  };

  const deactivateNoteHandler = id => {
    if (selectedFolder.name === 'Recently Deleted') return;
    dispatch(disableNotes());
    dispatch(
      deactivateNote({
        noteId: id,
        onSuccess: () => {
          setFilteredMenuItems(id);
          dispatch(enableNotes());
        },
        onError: () => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: 'Deactivate note error',
          });
          dispatch(enableNotes());
        },
      }),
    );
  };

  const deleteAllInactiveNotes = () => {
    confirm({
      content: 'These notes cannot be restored',
      onOk() {
        const ids = menuItems.map(item => item.id);
        dispatch(disableNotes());
        dispatch(
          deleteNotes({
            noteIds: ids,
            onSuccess: () => {
              setMenuItems([]);
              dispatch(enableNotes());
            },
            onError: () => {
              openNotificationWithIcon({
                type: NOTIFICATION_TYPES.ERROR,
                message: 'Ooops!',
                description: 'Delete notes error',
              });
              dispatch(enableNotes());
            },
          }),
        );
      },
    });
  };

  const deleteInactiveNote = id => {
    confirm({
      content: 'This note cannot be restored',
      onOk() {
        dispatch(disableNotes());
        dispatch(
          deleteNotes({
            noteIds: [id],
            onSuccess: () => {
              setFilteredMenuItems(id);
              dispatch(enableNotes());
            },
            onError: () => {
              openNotificationWithIcon({
                type: NOTIFICATION_TYPES.ERROR,
                message: 'Ooops!',
                description: 'Delete note error',
              });
              dispatch(enableNotes());
            },
          }),
        );
      },
    });
  };

  const setUpdatedItem = (id, updateDt, locked) => {
    setMenuItems(prev =>
      prev.map(item =>
        item.id === id
          ? {
              ...item,
              name: locked !== undefined ? item.name : noteName,
              updateDt: updateDt || item.updateDt,
              locked: locked !== undefined ? locked : item.locked,
              edit: false,
            }
          : item,
      ),
    );
  };

  const lockNote = (id, lockStatus) => {
    setIsLocking(true);
    dispatch(
      editNote({
        noteId: id,
        payload: {
          locked: !lockStatus,
        },
        onSuccess: data => {
          const locked = R.path(['data', 'locked'], data);
          const updateDt = R.path(['data', 'updateDt'], data);
          setUpdatedItem(id, updateDt, locked);
          setIsLocking(false);
        },
        onError: ({ data }) => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data?.data ? data.data : 'Lock note error',
          });
          setIsLocking(false);
        },
      }),
    );
  };

  const setNoteSelected = key => {
    dispatch(resetFolderDeletableState());
    dispatch(
      selectNote({
        selectedNote: {
          ...getItemByKey(menuItems, key || selectedKey),
          deletable: true,
          createNote: createNewNoteHandler,
          deleteNote: id => deactivateNoteHandler(id),
          setNotesMenuItems: setMenuItems,
          lockNote,
          deleteAllInactiveNotes,
          activateNoteHandler,
          deleteInactiveNote,
        },
      }),
    );
  };

  useEffect(() => {
    setNoteSelected();
    if (menuItems.length === 0) {
      dispatch(setFolderDeletableState());
      dispatch(resetNoteState());
      dispatch(
        selectNote({
          selectedNote: {
            createNote: createNewNoteHandler,
          },
        }),
      );
    }
  }, [menuItems]);

  const updateNote = id => {
    if (selectedFolder.name === 'Recently Deleted') return;

    const itemToEdit = menuItems.find(item => item.id === id);
    setMenuItems(prev =>
      prev.map(item => (item.id === id ? { ...item, edit: true } : item)),
    );
    setDefaultNoteName(itemToEdit.name);
    setNoteName(itemToEdit.name);
  };

  const saveNoteHandler = (id, e) => {
    if (e.keyCode && e.keyCode !== 13) return;

    const parent = e.target.parentElement;

    if (defaultNoteName === noteName) {
      setUpdatedItem(id);
      if (e.relatedTarget?.nodeName === 'LI' && e.relatedTarget !== parent) {
        e.relatedTarget.focus();
      } else if (parent) {
        parent.focus();
      }
      return;
    }

    dispatch(disableNotes());
    dispatch(
      editNote({
        noteId: id,
        payload: {
          name: noteName,
        },
        onSuccess: data => {
          const updateDt = R.path(['data', 'updateDt'], data);
          setUpdatedItem(id, updateDt);
          if (parent) {
            parent.focus();
          }
          dispatch(enableNotes());
        },
        onError: ({ data }) => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data?.data ? data.data : 'Edit note error',
          });
          dispatch(enableNotes());
        },
      }),
    );
  };

  const searchHandler = () =>
    dispatch(filterNotes({ name: search, folderId: selectedFolder.id }));

  const setSelectedHandler = key => {
    setNoteSelected(key);
    setSelectedKey(key);
  };

  return (
    <NotesList.Wrapper>
      {isDisabled && (
        <LoadingOverlay>
          <SettingFilled
            spin
            style={{ fontSize: 36, color: 'var(--hurricane)' }}
          />
        </LoadingOverlay>
      )}
      <SearchContainer style={{ filter: isDisabled ? 'opacity(0.7)' : 'none' }}>
        <Search
          size="small"
          placeholder="Filter"
          allowClear
          value={search}
          onChange={({ target: { value } }) => setSearch(value)}
          disabled={
            !selectedFolder.id || selectedFolder.name === 'Recently Deleted'
          }
          onSearch={searchHandler}
        />
      </SearchContainer>
      <Dropdown
        disabled={selectedFolder.name === 'Recently Deleted'}
        overlay={listContextMenu({
          createItem: createNewNoteHandler,
          deleteItem: () =>
            deactivateNoteHandler(getMenuItemId(menuItems, selectedKey)),
          editItem: () => updateNote(getMenuItemId(menuItems, selectedKey)),
          type: 'notes',
        })}
        trigger={['contextMenu']}
      >
        <NotesList.Menu
          selectedKeys={[selectedKey]}
          mode="inline"
          style={{ filter: isDisabled ? 'opacity(0.7)' : 'none' }}
        >
          {notes.state === FETCHING_STATE.FETCHING && (
            <MenuStatus disabled>
              <Spin size="small" />
            </MenuStatus>
          )}
          {notes.state === FETCHING_STATE.ERROR && (
            <MenuStatus disabled>
              <Alert message="Fetch error" type="error" />
            </MenuStatus>
          )}
          {notes.state === FETCHING_STATE.LOADED &&
            menuItems.map(item =>
              item.edit ? (
                <NotesList.MenuItem
                  disabled={isLocking && item.id !== selectedNote.id}
                  onClick={() => setSelectedHandler(item.key)}
                  key={item.key}
                  tabIndex={item.key}
                >
                  <NotesList.Input
                    ref={inputRef}
                    value={noteName}
                    onKeyUp={e => saveNoteHandler(item.id, e)}
                    onBlur={e => saveNoteHandler(item.id, e)}
                    onChange={({ target: { value } }) => setNoteName(value)}
                  />
                  <NotesList.MenuItemContent>
                    <NotesList.MenuItemTime>
                      {getNoteTime(item)}
                    </NotesList.MenuItemTime>
                    <NotesList.MenuItemAdditionalText>
                      {getNotePreviewText(item)}
                    </NotesList.MenuItemAdditionalText>
                  </NotesList.MenuItemContent>
                  <NotesList.LockStatus>
                    {item.locked ? (
                      <LockOutlined
                        spin={isLocking && item.id === selectedNote.id}
                      />
                    ) : (
                      <UnlockOutlined
                        spin={isLocking && item.id === selectedNote.id}
                      />
                    )}
                  </NotesList.LockStatus>
                </NotesList.MenuItem>
              ) : (
                <NotesList.MenuItem
                  disabled={isLocking && item.id !== selectedNote.id}
                  key={item.key}
                  tabIndex={item.key}
                  id={`${item.id}-notes-li`}
                  onKeyUp={e => {
                    if ([46, 8].includes(e.keyCode)) {
                      deactivateNoteHandler(item.id);
                    } else if (e.keyCode === 13) {
                      updateNote(item.id);
                    }
                  }}
                  onClick={() => setSelectedHandler(item.key)}
                >
                  <NotesList.MenuItemName>{item.name}</NotesList.MenuItemName>
                  <NotesList.MenuItemContent>
                    <NotesList.MenuItemTime>
                      {getNoteTime(item)}
                    </NotesList.MenuItemTime>
                    <NotesList.MenuItemAdditionalText>
                      {getNotePreviewText(item)}
                    </NotesList.MenuItemAdditionalText>
                  </NotesList.MenuItemContent>
                  <NotesList.LockStatus>
                    {item.locked ? (
                      <LockOutlined
                        spin={isLocking && item.id === selectedNote.id}
                      />
                    ) : (
                      <UnlockOutlined
                        spin={isLocking && item.id === selectedNote.id}
                      />
                    )}
                  </NotesList.LockStatus>
                </NotesList.MenuItem>
              ),
            )}
        </NotesList.Menu>
      </Dropdown>
    </NotesList.Wrapper>
  );
};

NotesList.Input = styled(Input)`
  background-color: inherit;
  border: none;
  line-height: 1;
  padding: 0;
  height: 16px;
`;

NotesList.LockStatus = styled.div`
  position: absolute;
  top: 10px;
  right: 0;
`;

NotesList.Menu = styled(Menu)`
  height: calc(100vh - 145px);
  overflow-y: auto;
  background-color: inherit;
  border: none;

  & .ant-menu-item-selected {
    background: linear-gradient(
      to left,
      var(--ebb) -300%,
      var(--merino)
    ) !important;
    background-color: inherit !important;
    color: var(--blue-zodiac) !important;
  }

  & .ant-menu-item-selected::after,
  & .ant-menu-item::after {
    border-right: none !important;
  }
`;

NotesList.MenuItem = styled(Menu.Item)`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100% !important;
  height: 80px !important;
  line-height: 1.2 !important;
  padding: 16px 16px 16px 24px !important;
  margin: 0 !important;
  outline: none;
  border-bottom: 1px solid var(--ebb);

  &:hover {
    color: var(--black);
    background: linear-gradient(to right, var(--alabaster), transparent 500%);
  }

  & .ant-input:focus,
  & .ant-input-focused {
    color: var(--black) !important;
    font-weight: 500;
    border: none;
    box-shadow: none;
  }
`;

NotesList.MenuItemAdditionalText = styled.span`
  color: var(--hurricane);
`;

NotesList.MenuItemContent = styled.div`
  text-overflow: ellipsis;
  overflow-x: hidden;
`;

NotesList.MenuItemName = styled.span`
  color: var(--black);
  font-weight: 500;
  text-overflow: ellipsis;
  overflow-x: hidden;
`;

NotesList.MenuItemTime = styled.span`
  color: var(--black);
  margin-right: 10px;
`;

NotesList.Wrapper = styled.div`
  padding-top: 20px;
  min-width: 142px;
  position: relative;
  text-align: center;
  width: 15%;
  background: linear-gradient(to bottom, var(--white), transparent 400%);
  border-right: 1px solid var(--hurricane);
`;

export default NotesList;
