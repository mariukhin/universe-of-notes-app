import { Alert, Button, Dropdown, Input, Menu, Spin } from 'antd';
import { PlusOutlined, SettingFilled } from '@ant-design/icons';

import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import {
  createFolder,
  deleteFolder,
  disableFolders,
  editFolder,
  enableFolders,
  fetchFolders,
  filterFolders,
  selectFolder,
} from 'Actions/folders';
import { resetNoteDeletableState } from 'Actions/notes';
import { getFolders, isFoldersDisabled } from 'Selectors/folders';
import { getUser } from 'Selectors/session';

import {
  getItemByKey,
  getMenuItemById,
  getMenuItemId,
  getMenuItems,
  getMenuItemsMaxKey,
  getNewMenuItemName,
  NOTIFICATION_TYPES,
  openNotificationWithIcon,
} from 'Utils';

import { confirm, listContextMenu } from 'Components';
import { FETCHING_STATE } from 'Services/reduxHelpers';

import {
  LoadingOverlay,
  MenuStatus,
  Search,
  SearchContainer,
} from './styledComponents';

const FoldersList = () => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);

  const [defaultFolderName, setDefaultFolderName] = useState('');
  const [folderName, setFolderName] = useState('');
  const [menuItems, setMenuItems] = useState([]);
  const [selectedKey, setSelectedKey] = useState('');
  const [triggeredTooltip, setTriggeredTooltip] = useState('');

  const folders = useSelector(getFolders);
  const user = useSelector(getUser);
  const isDisabled = useSelector(isFoldersDisabled);

  useEffect(() => {
    dispatch(fetchFolders());
  }, []);

  useEffect(() => {
    const newItems = getMenuItems(folders.data);
    setMenuItems(newItems);
    const firstItem = newItems[0];
    if (firstItem) {
      setSelectedKey(firstItem.key);
    }
  }, [folders.data]);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
      inputRef.current.select();
    }
  }, [menuItems]);

  const createNewFolderHandler = () => {
    const newItemProps = {
      key: String(menuItems.length > 0 ? getMenuItemsMaxKey(menuItems) + 1 : 1),
      edit: true,
    };
    dispatch(disableFolders());
    dispatch(
      createFolder({
        payload: {
          name: getNewMenuItemName(menuItems),
          userId: user.id,
        },
        onSuccess: ({ data }) => {
          setSelectedKey(newItemProps.key);
          setFolderName(data.name);
          setDefaultFolderName(data.name);
          const newItem = {
            ...data,
            ...newItemProps,
          };
          setMenuItems(prev => [newItem, ...prev]);
          dispatch(enableFolders());
        },
        onError: () => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: 'Create folder error',
          });
          dispatch(enableFolders());
        },
      }),
    );
  };

  const removeFolder = id => {
    const itemToDelete = getMenuItemById(menuItems, id);
    if (itemToDelete.readonly) return;

    confirm({
      content:
        'This folder cannot be restored. All notes in this folder will be deleted',
      onOk() {
        dispatch(disableFolders());
        dispatch(
          deleteFolder({
            folderId: id,
            onSuccess: () => {
              setMenuItems(prev => {
                let index = 0;
                const filteredItems = prev.filter((item, idx) => {
                  const condition = item.id !== id;
                  if (!condition) index = idx;
                  return condition;
                });
                if (filteredItems.length !== 0) {
                  const item = filteredItems[index];
                  if (item) {
                    setSelectedKey(item.key);
                    const suffix = '-folders-li';
                    const targetId = id + suffix;
                    const targetEl = document.getElementById(targetId);
                    const nextEl = targetEl && targetEl.nextSibling;
                    if (nextEl) {
                      nextEl.focus();
                    }
                  }
                }
                return filteredItems;
              });
              dispatch(enableFolders());
            },
            onError: () => {
              openNotificationWithIcon({
                type: NOTIFICATION_TYPES.ERROR,
                message: 'Ooops!',
                description: 'Delete folder error',
              });
              dispatch(enableFolders());
            },
          }),
        );
      },
    });
  };

  const setFolderSelected = () => {
    dispatch(resetNoteDeletableState());
    dispatch(
      selectFolder({
        selectedFolder: {
          ...getItemByKey(menuItems, selectedKey),
          deleteFolder: id => removeFolder(id),
          deletable: true,
        },
      }),
    );
  };

  useEffect(() => {
    setFolderSelected();
  }, [menuItems, selectedKey]);

  const updateFolder = id => {
    const itemToEdit = menuItems.find(item => item.id === id);
    if (itemToEdit.readonly) return;
    setMenuItems(prev =>
      prev.map(item => (item.id === id ? { ...item, edit: true } : item)),
    );
    setDefaultFolderName(itemToEdit.name);
    setFolderName(itemToEdit.name);
  };

  const getNameFit = (id, name) => {
    const textWidth = document.getElementById(`${id}-folders-text`).offsetWidth;
    const liWidth =
      document.getElementById(`${id}-folders-li`).clientWidth - 40;
    if (textWidth > liWidth) {
      setTriggeredTooltip(name);
    }
  };

  const setUpdatedItem = id => {
    setMenuItems(prev =>
      prev.map(item =>
        item.id === id ? { ...item, name: folderName, edit: false } : item,
      ),
    );
  };

  const saveFolderHandler = (id, e) => {
    if (e.keyCode && e.keyCode !== 13) return;

    const parent = e.target.parentElement;

    if (defaultFolderName === folderName) {
      setUpdatedItem(id);
      if (e.relatedTarget?.nodeName === 'LI' && e.relatedTarget !== parent) {
        e.relatedTarget.focus();
      } else if (parent) {
        parent.focus();
      }
      return;
    }

    dispatch(disableFolders());
    dispatch(
      editFolder({
        folderId: id,
        payload: {
          name: folderName,
        },
        onSuccess: () => {
          setUpdatedItem(id);
          if (parent) {
            parent.focus();
          }
          dispatch(enableFolders());
        },
        onError: ({ data }) => {
          openNotificationWithIcon({
            type: NOTIFICATION_TYPES.ERROR,
            message: 'Ooops!',
            description: data?.data ? data.data : 'Edit folder error',
          });
          dispatch(enableFolders());
        },
      }),
    );
  };

  const searchHandler = value => dispatch(filterFolders({ name: value }));

  const setSelectedHandler = key => {
    setFolderSelected();
    setSelectedKey(key);
  };

  return (
    <FoldersList.Wrapper>
      {isDisabled && (
        <LoadingOverlay>
          <SettingFilled
            spin
            style={{ fontSize: 36, color: 'var(--hurricane)' }}
          />
        </LoadingOverlay>
      )}
      <SearchContainer style={{ filter: isDisabled ? 'opacity(0.7)' : 'none' }}>
        <Search
          size="small"
          disabled={folders.state === FETCHING_STATE.FETCHING}
          placeholder="Filter"
          allowClear
          onSearch={searchHandler}
        />
      </SearchContainer>
      <Dropdown
        overlay={listContextMenu({
          deleteItem: () => removeFolder(getMenuItemId(menuItems, selectedKey)),
          editItem: () => updateFolder(getMenuItemId(menuItems, selectedKey)),
        })}
        trigger={['contextMenu']}
      >
        <FoldersList.Menu
          style={{ filter: isDisabled ? 'opacity(0.7)' : 'none' }}
          selectedKeys={[selectedKey]}
          mode="inline"
        >
          {folders.state === FETCHING_STATE.FETCHING && (
            <MenuStatus disabled>
              <Spin size="small" />
            </MenuStatus>
          )}
          {folders.state === FETCHING_STATE.ERROR && (
            <MenuStatus disabled>
              <Alert message="Fetch error" type="error" />
            </MenuStatus>
          )}
          {folders.state === FETCHING_STATE.LOADED &&
            menuItems.map(item =>
              item.edit ? (
                <FoldersList.MenuItem
                  onClick={() => setSelectedHandler(item.key)}
                  key={item.key}
                  tabIndex={item.key}
                >
                  <FoldersList.Input
                    ref={inputRef}
                    value={folderName}
                    onKeyUp={e => saveFolderHandler(item.id, e)}
                    onBlur={e => saveFolderHandler(item.id, e)}
                    onChange={({ target: { value } }) => setFolderName(value)}
                  />
                </FoldersList.MenuItem>
              ) : (
                <FoldersList.MenuItem
                  key={item.key}
                  tabIndex={item.key}
                  id={`${item.id}-folders-li`}
                  onMouseEnter={() => getNameFit(item.id, item.name)}
                  onMouseLeave={() => setTriggeredTooltip('')}
                  onKeyUp={e => {
                    if ([46, 8].includes(e.keyCode)) {
                      removeFolder(item.id);
                    } else if (e.keyCode === 13) {
                      updateFolder(item.id);
                    }
                  }}
                  onClick={() => {
                    getNameFit(item.id);
                    setSelectedHandler(item.key);
                  }}
                >
                  <span id={`${item.id}-folders-text`}>{item.name}</span>
                </FoldersList.MenuItem>
              ),
            )}
        </FoldersList.Menu>
      </Dropdown>
      {triggeredTooltip && (
        <FoldersList.MenuText>{triggeredTooltip}</FoldersList.MenuText>
      )}
      <FoldersList.Button
        onClick={createNewFolderHandler}
        style={{ filter: isDisabled ? 'opacity(0.7)' : 'none' }}
        disabled={folders.state === FETCHING_STATE.FETCHING}
        type="primary"
        icon={<PlusOutlined />}
      >
        New Folder
      </FoldersList.Button>
    </FoldersList.Wrapper>
  );
};

FoldersList.Button = styled(Button)`
  position: absolute;
  bottom: 10px;
  left: 10px;
  background-color: inherit;
  color: var(--blue-zodiac);
  border: none;
  box-shadow: none;
  text-shadow: none;

  &:focus {
    background-color: inherit;
    color: var(--blue-zodiac);
  }

  &:hover {
    background-color: inherit;
    color: var(--black);
  }
`;

FoldersList.Input = styled(Input)`
  background-color: inherit;
  border: none;
  padding: 0;
`;

FoldersList.Menu = styled(Menu)`
  height: calc(100vh - 258px);
  margin-bottom: 10px;
  overflow-y: auto;
  background-color: inherit;
  border: none;

  & .ant-menu-item-selected {
    background: linear-gradient(
      to right,
      var(--blue-zodiac),
      transparent 500%
    ) !important;
    background-color: inherit !important;
    color: var(--merino) !important;
  }

  & .ant-menu-item-selected::after,
  & .ant-menu-item::after {
    border-right: none !important;
  }
`;

FoldersList.MenuItem = styled(Menu.Item)`
  width: 100% !important;
  margin: 0 !important;
  outline: none;

  &:hover {
    color: var(--black);
    background: linear-gradient(to right, var(--white), transparent 500%);
  }

  & .ant-input:focus,
  & .ant-input-focused {
    color: var(--merino) !important;
    box-shadow: none;
  }
`;

FoldersList.MenuText = styled.p`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 52px;
  margin: 0;
  padding: 4px 10px;
  right: 10px;
  overflow-y: auto;
  background-color: var(--hurricane);
  color: var(--white);
`;

FoldersList.Wrapper = styled.div`
  padding-top: 20px;
  min-width: 142px;
  position: relative;
  height: 100%;
  background: linear-gradient(to bottom, var(--swiss-coffee), transparent 80%);
  border-right: 1px solid var(--hurricane);
`;

export default FoldersList;
