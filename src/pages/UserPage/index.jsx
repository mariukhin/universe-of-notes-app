import React, { useEffect } from 'react';
import { UnmountClosed } from 'react-collapse';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { resetFolderDeletableState, resetFolderState } from 'Actions/folders';
import { resetNoteState, setNoteDeletableState } from 'Actions/notes';
import { getFoldersListToggledState } from 'Selectors/global';

import FoldersList from './FoldersList';
import NoteEditor from './NoteEditor';
import NotesList from './NotesList';

const AppPage = () => {
  const dispatch = useDispatch();
  const isFoldersListCollapsed = useSelector(getFoldersListToggledState);

  useEffect(() => {
    return () => {
      dispatch(resetFolderState());
      dispatch(resetNoteState());
    };
  }, []);

  useEffect(() => {
    if (isFoldersListCollapsed) {
      dispatch(resetFolderDeletableState());
      dispatch(setNoteDeletableState());
    }
  }, [isFoldersListCollapsed]);

  return (
    <AppPage.Wrapper>
      <UnmountClosed isOpened={!isFoldersListCollapsed}>
        <FoldersList />
      </UnmountClosed>
      <NotesList />
      <NoteEditor isFoldersListCollapsed={isFoldersListCollapsed} />
    </AppPage.Wrapper>
  );
};

AppPage.Wrapper = styled.main`
  height: 100%;
  display: flex;

  & .ReactCollapse--collapse {
    min-width: 142px;
    width: 15%;
  }

  & .ReactCollapse--content {
    height: 100%;
  }
`;

export default AppPage;
