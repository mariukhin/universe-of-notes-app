import { Menu, Input } from 'antd';
import styled from 'styled-components';

export const MenuStatus = styled(Menu.Item)`
  width: 100% !important;
  margin: 0 !important;
  outline: none;
  padding: 0 !important;
  text-align: center;

  & .ant-spin-dot-item {
    background-color: var(--blue-zodiac);
  }
`;

export const Search = styled(Input.Search)`
  width: calc(100% - 40px);
  min-width: 120px;
  border-radius: 6px;

  &:hover {
    border: 1px solid var(--blue-zodiac);
  }
`;

export const SearchContainer = styled.div`
  margin-bottom: 15px;
  text-align: center;

  & .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border: 1px solid var(--blue-zodiac);
  }
`;

export const LoadingOverlay = styled.div`
  top: 0;
  height: 100%;
  width: 100%;
  position: absolute;
  z-index: 100;
  display: flex;
  align-items: center;
  justify-content: center;
`;
