import PropTypes from 'prop-types';

import { Divider, Input, Tooltip } from 'antd';
import {
  InfoCircleOutlined,
  KeyOutlined,
  MailOutlined,
  UserOutlined,
} from '@ant-design/icons';

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { register } from 'Actions/session';

import { NOTIFICATION_TYPES, openNotificationWithIcon } from 'Utils';

import { Form, SubmitButton, ToggleFormButton } from './styledComponents';

const RegisterForm = ({ redirectToApp, setSignInForm }) => {
  const dispatch = useDispatch();

  const [userData, setUserData] = useState({
    username: '',
    email: '',
    password: '',
  });

  const changeHandler = ({ target: { name, value } }) => {
    setUserData(prevData => ({
      ...prevData,
      [name]: value,
    }));
  };

  const submitHandler = e => {
    e.preventDefault();
    dispatch(
      register({
        payload: userData,
        onSuccess: () => redirectToApp(),
        onError: error => {
          const { data } = error;
          Object.values(data).forEach(value => {
            openNotificationWithIcon({
              type: NOTIFICATION_TYPES.ERROR,
              message: 'Ooops!',
              description: value[0],
            });
          });
        },
      }),
    );
  };

  return (
    <Form onSubmit={submitHandler}>
      <Divider style={{ margin: '0 0 20px' }} orientation="left">
        Register
      </Divider>
      <Input
        autoFocus
        required
        autoComplete="off"
        name="username"
        value={userData.username}
        onChange={changeHandler}
        style={{ marginBottom: 14 }}
        allowClear
        placeholder="Enter your username"
        prefix={<UserOutlined />}
        suffix={
          <Tooltip title="Username must be unique with a length of 8-14 characters">
            <InfoCircleOutlined />
          </Tooltip>
        }
      />
      <Input
        type="email"
        required
        autoComplete="off"
        name="email"
        value={userData.email}
        onChange={changeHandler}
        style={{ marginBottom: 14 }}
        allowClear
        placeholder="Enter your email"
        prefix={<MailOutlined />}
        suffix={
          <Tooltip title="Valid email required. Example: user1992@gmail.com">
            <InfoCircleOutlined />
          </Tooltip>
        }
      />
      <Input.Password
        type="password"
        required
        autoComplete="off"
        name="password"
        value={userData.password}
        onChange={changeHandler}
        style={{ marginBottom: 14 }}
        placeholder="Enter your password"
        prefix={<KeyOutlined />}
      />
      <SubmitButton htmlType="submit">Submit</SubmitButton>
      <div>
        <span>Already have login and password?</span>
        <ToggleFormButton type="link" onClick={setSignInForm}>
          Sign in
        </ToggleFormButton>
      </div>
    </Form>
  );
};

RegisterForm.propTypes = {
  redirectToApp: PropTypes.func.isRequired,
  setSignInForm: PropTypes.func.isRequired,
};

export default RegisterForm;
