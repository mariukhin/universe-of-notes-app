import { Button } from 'antd';

import styled from 'styled-components';

export const Form = styled.form`
  margin: 0 auto;
  min-width: 200px;
  max-width: 400px;

  & .ant-divider-with-text::after,
  .ant-divider-with-text::before {
    border-top: 1px solid var(--blue-zodiac);
  }

  & .ant-input-affix-wrapper:hover {
    border-color: var(--blue-zodiac);
  }

  & .ant-input-affix-wrapper-focused {
    border-color: var(--blue-zodiac);
    box-shadow: 0 0 0 2px rgba(16, 41, 71, 0.2);
  }

  @media (min-width: 768px) {
    margin: 0;
  }
`;

export const SubmitButton = styled(Button)`
  background-color: var(--blue-zodiac);
  border-color: var(--blue-zodiac);
  color: var(--white);
  margin-bottom: 20px;
  padding: 0 20px;

  &:focus,
  &:hover {
    color: var(--blue-zodiac);
    border: 1px solid var(--clam-shell);
    box-shadow: 0 0 0 2px var(--clam-shell);
  }
`;

export const ToggleFormButton = styled(Button)`
  & > span:hover {
    text-decoration: underline;
  }
`;
