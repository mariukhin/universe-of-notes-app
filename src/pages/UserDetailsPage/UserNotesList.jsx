import PropTypes from 'prop-types';

import { Menu, Pagination } from 'antd';
import { StarOutlined } from '@ant-design/icons';

import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import {
  getNotePreviewText,
  getNoteTime,
  USER_DETAILED_VIEW_MODES,
} from 'Utils';

import { NoItemsBox, PageLoader } from 'Components';

const defaultSelectedKey = '1';
const noItemsText = "This user doesn't have any notes";

const UserNotesList = ({
  currentPage,
  data,
  isNotesFetching,
  isNotesLoaded,
  setActiveNote,
  setCurrentPage,
  setOpenNoteViewer,
  setPage,
  totalCount,
  viewMode,
  likes,
  updateRating,
  updateRatingIdList,
}) => {
  const [selectedKey, setSelectedKey] = useState(defaultSelectedKey);

  useEffect(() => {
    const activeNote = data.find(item => item.key === selectedKey);
    setActiveNote(activeNote);
  }, [data, selectedKey]);

  const onChangePageHandler = page => {
    setCurrentPage(page);
    setPage(page);
    setSelectedKey(defaultSelectedKey);
  };

  const setSelectedHandler = key => {
    setSelectedKey(key);
    if (viewMode === USER_DETAILED_VIEW_MODES.ROW_MODE) {
      setOpenNoteViewer(true);
    }
  };

  const checkPrevLike = id => !!likes.find(like => like.noteId === Number(id));

  const updateRatingHandler = (e, item) => {
    e.stopPropagation();
    updateRating(item.id, !checkPrevLike(item.id));
  };

  return (
    <UserNotesList.Wrapper viewmode={viewMode}>
      {isNotesFetching ? (
        <PageLoader height="100%" />
      ) : isNotesLoaded && data.length === 0 ? (
        <NoItemsBox text={noItemsText} />
      ) : (
        <UserNotesList.Menu
          selectedKeys={[selectedKey]}
          mode="inline"
          viewmode={viewMode}
        >
          {data.map(item => (
            <UserNotesList.MenuItem
              onClick={() => setSelectedHandler(item.key)}
              key={item.key}
              tabIndex={item.key}
              viewmode={viewMode}
            >
              <UserNotesList.MenuItemName>
                {item.name}
              </UserNotesList.MenuItemName>
              <UserNotesList.MenuItemContent>
                <UserNotesList.MenuItemTime>
                  {getNoteTime(item)}
                </UserNotesList.MenuItemTime>
                <UserNotesList.MenuItemAdditionalText>
                  {getNotePreviewText(item)}
                </UserNotesList.MenuItemAdditionalText>
              </UserNotesList.MenuItemContent>
              <UserNotesList.Rating onClick={e => updateRatingHandler(e, item)}>
                <span style={{ marginRight: 4 }}>{item.rating}</span>
                <UserNotesList.Star
                  spin={updateRatingIdList.includes(item.id)}
                  isliked={checkPrevLike(item.id) ? 1 : 0}
                />
              </UserNotesList.Rating>
            </UserNotesList.MenuItem>
          ))}
        </UserNotesList.Menu>
      )}
      <UserNotesList.Pagination
        current={currentPage}
        onChange={onChangePageHandler}
        total={totalCount}
        pageSize={20}
        size="small"
        viewmode={viewMode}
        disabled={isNotesFetching}
      />
    </UserNotesList.Wrapper>
  );
};

UserNotesList.propTypes = {
  currentPage: PropTypes.number.isRequired,
  data: PropTypes.array.isRequired,
  isNotesFetching: PropTypes.bool.isRequired,
  isNotesLoaded: PropTypes.bool.isRequired,
  setActiveNote: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  setOpenNoteViewer: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  totalCount: PropTypes.number.isRequired,
  viewMode: PropTypes.string.isRequired,
  likes: PropTypes.array.isRequired,
  updateRating: PropTypes.func.isRequired,
  updateRatingIdList: PropTypes.array.isRequired,
};

UserNotesList.Menu = styled(Menu)`
  height: 100%;
  background-color: inherit;
  border: none;

  & .ant-menu-item-selected {
    background-color: var(--white) !important;
    color: var(--blue-zodiac) !important;
    box-shadow: 0 0 6px var(--clam-shell);
  }

  & .ant-menu-item-selected::after,
  & .ant-menu-item::after {
    border-right: none !important;
  }

  display: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'flex' : 'block'};
  flex-wrap: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'wrap' : 'nowrap'};
  align-content: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'flex-start' : 'normal'};
  margin: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '0 auto' : '0'};
  width: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '792px' : 'auto'};
  overflow-y: auto;
  padding: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
      ? '20px 20px 6px'
      : '4px 20px'};

  @media (min-width: 1200px) {
    width: ${({ viewmode }) =>
      viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '1192px' : 'auto'};
  }
`;

UserNotesList.MenuItem = styled(Menu.Item)`
  cursor: pointer;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
      ? '340px !important'
      : '100% !important'};
  height: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
      ? '110px !important'
      : '90px !important'};
  line-height: 1.2 !important;
  padding: 16px 16px 16px 24px !important;
  outline: none;
  background-color: var(--white);
  border-radius: 10px;

  @media (max-width: 1199px) {
    &:not(:nth-child(2n)) {
      margin-right: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '60px' : '0'};
    }

    &:nth-last-child(n + 3) {
      margin-bottom: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
          ? '50px !important'
          : '20px !important'};
    }

    &:not(:nth-last-child(n + 3)) {
      margin-bottom: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
          ? '0 !important'
          : '20px !important'};
    }
  }

  @media (min-width: 1200px) {
    &:not(:nth-child(3n)) {
      margin-right: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '60px' : '0'};
    }

    &:nth-last-child(n + 4) {
      margin-bottom: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
          ? '50px !important'
          : '20px !important'};
    }

    &:not(:nth-last-child(n + 4)) {
      margin-bottom: ${({ viewmode }) =>
        viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
          ? '0 !important'
          : '20px !important'};
    }
  }

  &:hover {
    color: var(--black);
    box-shadow: 0 0 10px var(--clam-shell);
  }

  &:active {
    background: var(--white);
  }
`;

UserNotesList.MenuItemAdditionalText = styled.span`
  color: var(--hurricane);
`;

UserNotesList.MenuItemContent = styled.div`
  text-overflow: ellipsis;
  overflow-x: hidden;
`;

UserNotesList.MenuItemName = styled.span`
  color: var(--black);
  font-weight: 500;
  text-overflow: ellipsis;
  overflow-x: hidden;
  margin-bottom: 10px;
`;

UserNotesList.MenuItemTime = styled.span`
  color: var(--black);
  margin-right: 10px;
`;

UserNotesList.Pagination = styled(Pagination)`
  display: flex;
  position: absolute;
  bottom: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '8px' : '2px'};
  left: 50%;
  transform: translateX(-50%);

  & .ant-pagination-options {
    display: none;
  }

  & .ant-pagination-item-active {
    background: inherit;
    border-color: var(--blue-zodiac);

    & > a {
      color: var(--blue-zodiac);
    }
  }

  & .ant-pagination-item:hover a,
  & .ant-pagination-prev:hover a,
  & .ant-pagination-next:hover a {
    color: var(--black);
  }

  & .ant-pagination-item-link-icon {
    color: var(--blue-zodiac) !important;
  }
`;

UserNotesList.Rating = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  z-index: 1;
`;

UserNotesList.Star = styled(StarOutlined)`
  svg {
    fill: ${props => props.isliked && 'var(--like)'} !important;
  }
`;

UserNotesList.Wrapper = styled.div`
  position: relative;
  padding: 10px 0 38px;
  height: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '100%' : 'auto'};
  flex-basis: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '100%' : '24%'};
  min-width: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'none' : '300px'};
  max-width: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? 'none' : '400px'};
`;

export default UserNotesList;
