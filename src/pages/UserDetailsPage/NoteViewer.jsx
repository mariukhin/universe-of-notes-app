import PropTypes from 'prop-types';

import { convertFromRaw, EditorState } from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import plugins from 'Pages/UserPage/NoteEditor/plugins';
import {
  getBlockStyle,
  getColorsStyleMap,
  pickerColors,
} from 'Pages/UserPage/NoteEditor/utils';
import { getParseErrorContent, USER_DETAILED_VIEW_MODES } from 'Utils';

const NoteViewer = ({ activeNote, viewMode }) => {
  const [editorState, setEditorState] = useState(null);

  useEffect(() => {
    if (activeNote?.content) {
      try {
        setEditorState(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(activeNote.content)),
          ),
        );
      } catch (error) {
        setEditorState(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(getParseErrorContent(error))),
          ),
        );
      }
    } else {
      setEditorState(EditorState.createEmpty());
    }
  }, [activeNote]);

  return (
    <NoteViewer.Wrapper viewmode={viewMode}>
      {activeNote && editorState && (
        <div>
          <Editor
            onChange={() => null}
            blockStyleFn={getBlockStyle}
            editorState={editorState}
            plugins={plugins}
            readOnly
            customStyleMap={getColorsStyleMap(pickerColors)}
          />
        </div>
      )}
    </NoteViewer.Wrapper>
  );
};

NoteViewer.defaultProps = {
  activeNote: null,
};

NoteViewer.propTypes = {
  activeNote: PropTypes.object,
  viewMode: PropTypes.string.isRequired,
};

NoteViewer.Wrapper = styled.div`
  padding: ${({ viewmode }) =>
    viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE ? '0' : '10px 40px'};
  flex-basis: 76%;
  min-width: 800px;

  & > div {
    overflow-y: auto;
    padding: 20px;
    border: 1px solid var(--swiss-coffee);
    border-radius: 10px;
    height: ${({ viewmode }) =>
      viewmode === USER_DETAILED_VIEW_MODES.ROW_MODE
        ? 'calc(100vh - 200px)'
        : '100%'};
    background-color: var(--white);
  }

  & .draftJsFocusPlugin__focused__3Mksn {
    box-shadow: none;
  }

  & .draftJsFocusPlugin__unfocused__1Wvrs:hover {
    box-shadow: none;
  }
`;

export default NoteViewer;
