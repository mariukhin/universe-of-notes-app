import PropTypes from 'prop-types';

import { Modal } from 'antd';

import React from 'react';

import NoteViewer from './NoteViewer';

const NoteViewerModal = ({ activeNote, open, setOpen, viewMode }) => {
  const closeHandler = () => setOpen(false);

  return (
    <Modal
      centered
      width={900}
      visible={open}
      title={activeNote ? activeNote.name : ''}
      footer={null}
      onCancel={closeHandler}
    >
      <NoteViewer activeNote={activeNote} viewMode={viewMode} />
    </Modal>
  );
};

NoteViewerModal.defaultProps = {
  activeNote: null,
};

NoteViewerModal.propTypes = {
  activeNote: PropTypes.object,
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  viewMode: PropTypes.string.isRequired,
};

export default NoteViewerModal;
