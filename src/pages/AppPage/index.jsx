import PropTypes from 'prop-types';

import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';

import { routePaths } from 'Routes';
import { Footer, Header } from 'Components';
import MainPageHeader from 'Pages/MainPage/Content/MainPageHeader';
import { ACTIVE_PAGES, isMainHeader } from 'Utils';

const AppPage = ({ children }) => {
  const location = useLocation();

  const [activePage, setActivePage] = useState('');

  useEffect(() => {
    switch (location.pathname) {
      case routePaths.main:
        setActivePage(ACTIVE_PAGES.MAIN_PAGE);
        break;
      case routePaths.user:
        setActivePage(ACTIVE_PAGES.USER_PAGE);
        break;
      default:
        if (location.pathname.startsWith(routePaths.userDetails.slice(0, 17))) {
          setActivePage(ACTIVE_PAGES.USER_DETAILS_PAGE);
        } else if (
          location.pathname.startsWith(routePaths.noteDetails.slice(0, 17))
        ) {
          setActivePage(ACTIVE_PAGES.NOTE_DETAILS_PAGE);
        } else setActivePage('');
    }
  }, [location]);

  return (
    <AppPage.Wrapper>
      {activePage === ACTIVE_PAGES.USER_PAGE && <Header />}
      {isMainHeader(activePage) && <MainPageHeader />}
      {children}
      <Footer />
    </AppPage.Wrapper>
  );
};

AppPage.propTypes = {
  children: PropTypes.element.isRequired,
};

AppPage.Wrapper = styled.div`
  display: grid;
  grid-template-rows: auto 1fr auto;
  height: 100vh;
`;

export default AppPage;
