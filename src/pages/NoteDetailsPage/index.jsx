import { StarTwoTone } from '@ant-design/icons';
import { convertFromRaw, EditorState } from 'draft-js';
import Editor from 'draft-js-plugins-editor';

import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, Link } from 'react-router-dom';
import styled from 'styled-components';

import {
  getNote,
  resetNoteState,
  updateNoteRating,
  fetchLikes,
} from 'Actions/notes';
import { getSelectedNote, getLikes } from 'Selectors/notes';

import { PageLoader } from 'Components';

import plugins from 'Pages/UserPage/NoteEditor/plugins';
import {
  getBlockStyle,
  getColorsStyleMap,
  pickerColors,
} from 'Pages/UserPage/NoteEditor/utils';
import { FavoriteNumBlock } from 'Pages/MainPage/styledComponents';

import { FETCHING_STATE } from 'Services/reduxHelpers';
import { getNoteDate, getParseErrorContent } from 'Utils';

const NoteDetailsPage = () => {
  const dispatch = useDispatch();
  const note = useSelector(getSelectedNote);
  const likes = useSelector(getLikes);
  const [editorState, setEditorState] = useState(null);
  const [isLike, setIsLike] = useState(false);
  const [cardRating, setCardRating] = useState(null);
  const { noteId } = useParams();

  const isNoteFetching = note.state === FETCHING_STATE.FETCHING;

  useEffect(() => {
    return () => {
      dispatch(resetNoteState());
    };
  }, []);

  useEffect(() => {
    dispatch(getNote({ noteId }));
    dispatch(fetchLikes());
  }, []);

  useEffect(() => {
    if (note?.content) {
      try {
        setEditorState(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(note.content)),
          ),
        );
        setCardRating(note.rating);
      } catch (error) {
        setEditorState(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(getParseErrorContent(error))),
          ),
        );
      }
    } else {
      setEditorState(EditorState.createEmpty());
    }
  }, [note]);

  useEffect(() => {
    if (likes.state === FETCHING_STATE.LOADED) {
      setIsLike(!!likes.data.find(like => like.noteId === Number(noteId)));
    }
  }, [likes]);

  const toggleLikeButton = isLiked => {
    dispatch(
      updateNoteRating({
        noteId,
        payload: {
          rating: isLiked ? 1 : 0,
        },
      }),
    );
  };

  return (
    <NoteDetailsPage.Wrapper>
      {isNoteFetching ? (
        <PageLoader height="calc(100vh - 233px)" />
      ) : (
        <NoteDetailsPage.Block>
          {note && (
            <>
              <NoteDetailsPage.NoteTitle>{note.name}</NoteDetailsPage.NoteTitle>
              <NoteDetailsPage.NoteViewer>
                {editorState && (
                  <div>
                    <Editor
                      onChange={() => null}
                      blockStyleFn={getBlockStyle}
                      editorState={editorState}
                      plugins={plugins}
                      readOnly
                      customStyleMap={getColorsStyleMap(pickerColors)}
                    />
                  </div>
                )}
              </NoteDetailsPage.NoteViewer>
              <NoteDetailsPage.Footer>
                <NoteDetailsPage.NoteData>
                  {getNoteDate(note, true)}
                </NoteDetailsPage.NoteData>
                <NoteDetailsPage.RatingContainer
                  isliked={isLike ? 1 : 0}
                  onClick={() => {
                    toggleLikeButton(!isLike);
                    setIsLike(!isLike);
                    setCardRating(!isLike ? cardRating + 1 : cardRating - 1);
                  }}
                >
                  <StarTwoTone twoToneColor="var(--clam-shell)" />
                  <p>{cardRating}</p>
                </NoteDetailsPage.RatingContainer>
                <NoteDetailsPage.NoteAuthor
                  to={`/app/user-details/${note.author}`}
                >
                  {note.author}
                </NoteDetailsPage.NoteAuthor>
              </NoteDetailsPage.Footer>
            </>
          )}
        </NoteDetailsPage.Block>
      )}
    </NoteDetailsPage.Wrapper>
  );
};

NoteDetailsPage.Wrapper = styled.div`
  height: calc(100vh - 133px);
`;

NoteDetailsPage.Block = styled.section`
  width: 60%;
  margin: 0 auto;
  margin-top: 30px;
`;

NoteDetailsPage.NoteViewer = styled.div`
  flex-basis: 76%;
  min-width: 800px;
  padding-bottom: 10px;

  & > div {
    overflow-y: auto;
    padding: 20px;
    border: 1px solid var(--swiss-coffee);
    border-radius: 10px;
    height: calc(100vh - 270px);
    background-color: var(--white);
  }

  & .draftJsFocusPlugin__focused__3Mksn {
    box-shadow: none;
  }

  & .draftJsFocusPlugin__unfocused__1Wvrs:hover {
    box-shadow: none;
  }
`;

NoteDetailsPage.NoteTitle = styled.p`
  font-size: 20px;
  color: var(--blue-zodiac);
  font-family: 'Montserrat';
  text-align: center;
  padding-bottom: 20px;
`;

NoteDetailsPage.CreatedBy = styled.p`
  font-size: 15px;
  color: var(--blue-zodiac);
  font-family: 'Roboto';
  font-style: italic;
`;

NoteDetailsPage.Footer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 95%;
  margin: 0 auto;
`;

NoteDetailsPage.NoteData = styled.p`
  font-size: 17px;
  color: var(--blue-zodiac);
  font-family: 'Roboto';
  font-weight: 400;
  line-height: 37px;
`;

NoteDetailsPage.NoteAuthor = styled(Link)`
  font-size: 17px;
  color: var(--blue-zodiac);
  font-family: 'Roboto';
  cursor: pointer;
  line-height: 37px;

  &::before {
    content: 'created by - ';
    font-size: 15px;
    color: var(--blue-zodiac);
    font-family: 'Roboto';
    font-style: italic;
  }

  &:hover {
    color: var(--clam-shell);
  }
`;

NoteDetailsPage.RatingContainer = styled(FavoriteNumBlock)`
  width: 6%;

  .anticon-star {
    cursor: pointer;

    svg {
      height: 40px;
      width: 43px;
    }
  }

  p {
    margin-left: 6px;
    font-size: 17px;
    line-height: 41px;
  }
`;

export default NoteDetailsPage;
