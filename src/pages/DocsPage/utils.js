import PropTypes from 'prop-types';

import React from 'react';

import addEmojiActionImg from 'Assets/docs/add-emoji-action.png';
import addEmojiResultImg from 'Assets/docs/add-emoji-result.png';
import addImageActionImg from 'Assets/docs/add-image-action.png';
import addImageResultImg from 'Assets/docs/add-image-result.png';
import addVideoActionImg from 'Assets/docs/add-video-action.png';
import addVideoLinkImg from 'Assets/docs/add-video-link.png';
import addVideoResultImg from 'Assets/docs/add-video-result.png';
import alignCenterActionImg from 'Assets/docs/align-center-action.png';
import alignCenterResultImg from 'Assets/docs/align-center-result.png';
import alignLeftActionImg from 'Assets/docs/align-left-action.png';
import alignLeftResultImg from 'Assets/docs/align-left-result.png';
import alignRightActionImg from 'Assets/docs/align-right-action.png';
import alignRightResultImg from 'Assets/docs/align-right-result.png';
import boldActionImg from 'Assets/docs/bold-action.png';
import boldResultImg from 'Assets/docs/bold-result.png';
import codeActionImg from 'Assets/docs/code-action.png';
import codeResultImg from 'Assets/docs/code-result.png';
import createNoteImg from 'Assets/docs/create-note.png';
import deleteAllNotesImg from 'Assets/docs/delete-all-notes.png';
import deleteItemImg from 'Assets/docs/delete-item.png';
import headersActionImg from 'Assets/docs/headers-action.png';
import headersResultImg from 'Assets/docs/headers-result.png';
import italicActionImg from 'Assets/docs/italic-action.png';
import italicResultImg from 'Assets/docs/italic-result.png';
import lockNoteImg from 'Assets/docs/lock-note.png';
import logoutImg from 'Assets/docs/logout.png';
import orderedListItemActionImg from 'Assets/docs/ordered-list-item-action.png';
import orderedListItemResultImg from 'Assets/docs/ordered-list-item-result.png';
import readonlyActionImg from 'Assets/docs/readonly-action.png';
import redoActionImg from 'Assets/docs/redo-action.png';
import saveActionImg from 'Assets/docs/save-action.png';
import setColorActionImg from 'Assets/docs/set-color-action.png';
import setColorResultImg from 'Assets/docs/set-color-result.png';
import setColorSelectImg from 'Assets/docs/set-color-select.png';
import strikethroughActionImg from 'Assets/docs/strikethrough-action.png';
import strikethroughResultImg from 'Assets/docs/strikethrough-result.png';
import toggleFoldersVisibilityImg from 'Assets/docs/toggle-folders-visibility.png';
import underlineActionImg from 'Assets/docs/underline-action.png';
import underlineResultImg from 'Assets/docs/underline-result.png';
import undoActionImg from 'Assets/docs/undo-action.png';
import unorderedListItemActionImg from 'Assets/docs/unordered-list-item-action.png';
import unorderedListItemResultImg from 'Assets/docs/unordered-list-item-result.png';
import userSettingsImg from 'Assets/docs/user-settings.png';

import {
  UserPage,
  UserPageFoldersList,
  UserPageFoldersListCreate,
  UserPageFoldersListDelete,
  UserPageFoldersListEdit,
  UserPageFoldersListSearch,
  UserPageNotesList,
  UserPageNotesListCreate,
  UserPageNotesListDelete,
  UserPageNotesListEdit,
  UserPageNotesListSearch,
  UserPageNotesListRecentlyDeleted,
  UserPageEditor,
  UserPageEditorActions,
  UserPageEditorPlugins,
  UserPageActions,
} from './Content';

export const DOCS_PAGES = Object.freeze({
  USER_PAGE: 'user-page',
  USER_PAGE_FOLDERS_LIST: 'user-page-folders-list',
  USER_PAGE_FOLDERS_LIST_CREATE: 'user-page-folders-list-create',
  USER_PAGE_FOLDERS_LIST_DELETE: 'user-page-folders-list-delete',
  USER_PAGE_FOLDERS_LIST_EDIT: 'user-page-folders-list-edit',
  USER_PAGE_FOLDERS_LIST_SEARCH: 'user-page-folders-list-search',
  USER_PAGE_NOTES_LIST: 'user-page-notes-list',
  USER_PAGE_NOTES_LIST_CREATE: 'user-page-notes-list-create',
  USER_PAGE_NOTES_LIST_DELETE: 'user-page-notes-list-delete',
  USER_PAGE_NOTES_LIST_EDIT: 'user-page-notes-list-edit',
  USER_PAGE_NOTES_LIST_SEARCH: 'user-page-notes-list-search',
  USER_PAGE_NOTES_LIST_RECENTLY_DELETED:
    'user-page-notes-list-recently-deleted',
  USER_PAGE_EDITOR: 'user-page-editor',
  USER_PAGE_EDITOR_ACTIONS: 'user-page-editor-actions',
  USER_PAGE_EDITOR_PLUGINS: 'user-page-editor-plugins',
  USER_PAGE_ACTIONS: 'user-page-actions',
});

export const treeData = [
  {
    title: 'User page',
    key: DOCS_PAGES.USER_PAGE,
    children: [
      {
        title: 'Folders List',
        key: DOCS_PAGES.USER_PAGE_FOLDERS_LIST,
        children: [
          {
            title: 'Create Folder',
            key: DOCS_PAGES.USER_PAGE_FOLDERS_LIST_CREATE,
            isLeaf: true,
          },
          {
            title: 'Delete Folder',
            key: DOCS_PAGES.USER_PAGE_FOLDERS_LIST_DELETE,
            isLeaf: true,
          },
          {
            title: 'Edit Folder',
            key: DOCS_PAGES.USER_PAGE_FOLDERS_LIST_EDIT,
            isLeaf: true,
          },
          {
            title: 'Search Folder',
            key: DOCS_PAGES.USER_PAGE_FOLDERS_LIST_SEARCH,
            isLeaf: true,
          },
        ],
      },
      {
        title: 'Notes List',
        key: DOCS_PAGES.USER_PAGE_NOTES_LIST,
        children: [
          {
            title: 'Create Note',
            key: DOCS_PAGES.USER_PAGE_NOTES_LIST_CREATE,
            isLeaf: true,
          },
          {
            title: 'Delete Note',
            key: DOCS_PAGES.USER_PAGE_NOTES_LIST_DELETE,
            isLeaf: true,
          },
          {
            title: 'Edit Note',
            key: DOCS_PAGES.USER_PAGE_NOTES_LIST_EDIT,
            isLeaf: true,
          },
          {
            title: 'Search Note',
            key: DOCS_PAGES.USER_PAGE_NOTES_LIST_SEARCH,
            isLeaf: true,
          },
          {
            title: 'Recently Deleted',
            key: DOCS_PAGES.USER_PAGE_NOTES_LIST_RECENTLY_DELETED,
            isLeaf: true,
          },
        ],
      },
      {
        title: 'Editor',
        key: DOCS_PAGES.USER_PAGE_EDITOR,
        children: [
          {
            title: 'Actions',
            key: DOCS_PAGES.USER_PAGE_EDITOR_ACTIONS,
            isLeaf: true,
          },
          {
            title: 'Plugins',
            key: DOCS_PAGES.USER_PAGE_EDITOR_PLUGINS,
            isLeaf: true,
          },
        ],
      },
      {
        title: 'User Actions',
        key: DOCS_PAGES.USER_PAGE_ACTIONS,
        isLeaf: true,
      },
    ],
  },
];

const constructGetTitle = () => {
  let title = '';

  return function getTitle(tree, key) {
    // eslint-disable-next-line no-restricted-syntax
    for (const item of tree) {
      if (item.key === key) {
        title = item.title;
        break;
      }
      const { children } = item;
      if (children) {
        title = getTitle(item.children, key);
      }
    }
    return title;
  };
};

export const getTitleByKey = key => constructGetTitle()(treeData, key);

export const getContent = ({ key }) => {
  switch (key) {
    case DOCS_PAGES.USER_PAGE:
      return <UserPage />;
    case DOCS_PAGES.USER_PAGE_FOLDERS_LIST:
      return <UserPageFoldersList />;
    case DOCS_PAGES.USER_PAGE_FOLDERS_LIST_CREATE:
      return <UserPageFoldersListCreate />;
    case DOCS_PAGES.USER_PAGE_FOLDERS_LIST_DELETE:
      return <UserPageFoldersListDelete />;
    case DOCS_PAGES.USER_PAGE_FOLDERS_LIST_EDIT:
      return <UserPageFoldersListEdit />;
    case DOCS_PAGES.USER_PAGE_FOLDERS_LIST_SEARCH:
      return <UserPageFoldersListSearch />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST:
      return <UserPageNotesList />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST_CREATE:
      return <UserPageNotesListCreate />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST_DELETE:
      return <UserPageNotesListDelete />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST_EDIT:
      return <UserPageNotesListEdit />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST_SEARCH:
      return <UserPageNotesListSearch />;
    case DOCS_PAGES.USER_PAGE_NOTES_LIST_RECENTLY_DELETED:
      return <UserPageNotesListRecentlyDeleted />;
    case DOCS_PAGES.USER_PAGE_EDITOR:
      return <UserPageEditor />;
    case DOCS_PAGES.USER_PAGE_EDITOR_ACTIONS:
      return <UserPageEditorActions />;
    case DOCS_PAGES.USER_PAGE_EDITOR_PLUGINS:
      return <UserPageEditorPlugins />;
    case DOCS_PAGES.USER_PAGE_ACTIONS:
      return <UserPageActions />;
    default:
      return <UserPage />;
  }
};

getContent.propTypes = {
  key: PropTypes.string.isRequired,
};

export const stylingActionsConfig = [
  {
    actionName: 'Bold',
    actionImg: boldActionImg,
    resultImg: boldResultImg,
  },
  {
    actionName: 'Italic',
    actionImg: italicActionImg,
    resultImg: italicResultImg,
  },
  {
    actionName: 'Underline',
    actionImg: underlineActionImg,
    resultImg: underlineResultImg,
  },
  {
    actionName: 'Code',
    actionImg: codeActionImg,
    resultImg: codeResultImg,
  },
  {
    actionName: 'Strikethrough',
    actionImg: strikethroughActionImg,
    resultImg: strikethroughResultImg,
  },
  {
    actionName: 'Align Left',
    actionImg: alignLeftActionImg,
    resultImg: alignLeftResultImg,
    resultImgWidth: 90,
  },
  {
    actionName: 'Align Center',
    actionImg: alignCenterActionImg,
    resultImg: alignCenterResultImg,
    resultImgWidth: 90,
  },
  {
    actionName: 'Align Right',
    actionImg: alignRightActionImg,
    resultImg: alignRightResultImg,
    resultImgWidth: 90,
  },
  {
    actionName: 'Ordered List',
    actionImg: orderedListItemActionImg,
    resultImg: orderedListItemResultImg,
    block: true,
    resultImgWidth: 90,
  },
  {
    actionName: 'Unordered List',
    actionImg: unorderedListItemActionImg,
    resultImg: unorderedListItemResultImg,
    block: true,
    resultImgWidth: 90,
  },
  {
    actionName: 'Headers',
    actionImg: headersActionImg,
    resultImg: headersResultImg,
    block: true,
    actionImgWidth: 110,
  },
  {
    actionName: 'Add Image',
    actionImg: addImageActionImg,
    resultImg: addImageResultImg,
    block: true,
    additionalAction: { text: 'Select image.' },
    resultImgWidth: 110,
  },
  {
    actionName: 'Set Color',
    actionImg: setColorActionImg,
    resultImg: setColorResultImg,
    additionalAction: {
      text: 'Select color.',
      img: setColorSelectImg,
      imgWidth: 140,
    },
    resultImgWidth: 90,
  },
  {
    actionName: 'Add Video',
    actionImg: addVideoActionImg,
    resultImg: addVideoResultImg,
    block: true,
    additionalAction: {
      text: 'Add video link.',
      img: addVideoLinkImg,
      imgWidth: 200,
    },
    resultImgWidth: 220,
  },
  {
    actionName: 'Add Emoji',
    actionImg: addEmojiActionImg,
    resultImg: addEmojiResultImg,
    block: true,
    additionalAction: { text: 'Select emoji.' },
    resultImgWidth: 40,
  },
];

export const otherActionsConfig = [
  {
    actionName: 'Undo',
    actionImg: undoActionImg,
    actionDescription: 'Returns the previous state of the editor.',
  },
  {
    actionName: 'Redo',
    actionImg: redoActionImg,
    actionDescription: 'Returns the next state of the editor.',
  },
  {
    actionName: 'Readonly',
    actionImg: readonlyActionImg,
    actionDescription: 'Switches the editor to a note in reading mode.',
  },
  {
    actionName: 'Save',
    actionImg: saveActionImg,
    actionDescription: 'Saves the current state of the editor.',
  },
];

export const pluginsList = [
  {
    pluginName: 'Emoji',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/emoji',
  },
  {
    pluginName: 'Image',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/image',
  },
  {
    pluginName: 'Video',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/video',
  },
  {
    pluginName: 'Hashtag',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/hashtag',
  },
  {
    pluginName: 'Undo',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/undo',
  },
  {
    pluginName: 'Counter',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/counter',
  },
  {
    pluginName: 'Linkify',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/linkify',
  },
  {
    pluginName: 'Focus',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/focus',
  },
  {
    pluginName: 'Alignment',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/alignment',
  },
  {
    pluginName: 'Resizable',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/resizeable',
  },
  {
    pluginName: 'Drag`n`Drop',
    pluginSrc: 'https://www.draft-js-plugins.com/plugin/drag-n-drop',
  },
];

export const userPageActionsList = [
  {
    actionName: 'Toggle folders visibility',
    actionContent: 'Toggle folders visibility',
    actionImg: toggleFoldersVisibilityImg,
  },
  {
    actionName: 'Delete item',
    actionContent: 'Deletes a folder permanently or a note temporarily',
    actionImg: deleteItemImg,
  },
  {
    actionName: 'Create note',
    actionContent: 'Create note',
    actionImg: createNoteImg,
  },
  {
    actionName: 'Lock note',
    actionContent:
      'After locking, the note is available in read-only mode and is not available to other users',
    actionImg: lockNoteImg,
  },
  {
    actionName: 'Delete all notes',
    actionContent:
      'Only available in recently deleted. Deletes all notes permanently',
    actionImg: deleteAllNotesImg,
  },
  {
    actionName: 'User Settings',
    actionContent:
      'Provides the ability for the user to edit personal settings',
    actionImg: userSettingsImg,
  },
  {
    actionName: 'Logout',
    actionContent: 'Logout',
    actionImg: logoutImg,
  },
];
