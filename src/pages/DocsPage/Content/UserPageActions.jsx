import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import { userPageActionsList } from '../utils';

import { InlineImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageActions = () => (
  <>
    <Paragraph>The user has the opportunity to use such actions:</Paragraph>
    <UserPageActions.OL>
      {userPageActionsList.map(item => (
        <li key={item.actionName}>
          <UserPageActions.LiGrouper>
            <InlineImageContainer width={40} style={{ margin: '0 10px' }}>
              <LazyImage src={item.actionImg} alt={item.actionName} />
            </InlineImageContainer>
            <LiContent>&#x2014; &nbsp; {item.actionContent}.</LiContent>
          </UserPageActions.LiGrouper>
        </li>
      ))}
    </UserPageActions.OL>
  </>
);

UserPageActions.LiGrouper = styled.div`
  display: flex;
  align-items: baseline;
`;

UserPageActions.OL = styled.ol`
  padding-left: 70px;
`;

export default UserPageActions;
