import React from 'react';

import { LazyImage } from 'Components';

import userPageImg from 'Assets/docs/user-page.png';

import { ImageContainer, Paragraph } from './styledComponents';

const UserPage = () => (
  <>
    <Paragraph>
      User Page provides user ability to manage notes with full editor
      capabilities, convenient structured storage and many other possibilities.
    </Paragraph>
    <ImageContainer width={900}>
      <LazyImage src={userPageImg} alt="User Page" />
    </ImageContainer>
  </>
);

export default UserPage;
