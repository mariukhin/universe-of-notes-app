import React from 'react';
import styled from 'styled-components';

import { pluginsList } from '../utils';

import { DiskUL, Paragraph, ULli } from './styledComponents';

const UserPageEditorPlugins = () => (
  <>
    <Paragraph>
      The application uses plugins from{' '}
      <UserPageEditorPlugins.Link
        href="https://www.draft-js-plugins.com/"
        target="_blank"
      >
        https://www.draft-js-plugins.com
      </UserPageEditorPlugins.Link>
      .
    </Paragraph>
    <Paragraph>Plugins list:</Paragraph>
    <DiskUL>
      {pluginsList.map(item => (
        <ULli key={item.pluginName}>
          {item.pluginName} plugin:{' '}
          <UserPageEditorPlugins.Link href={item.pluginSrc} target="_blank">
            {item.pluginSrc}
          </UserPageEditorPlugins.Link>
          .
        </ULli>
      ))}
    </DiskUL>
  </>
);

UserPageEditorPlugins.Link = styled.a`
  line-height: 1;
  color: var(--blue-zodiac);
  font-weight: 500;

  &:hover {
    color: var(--blue-zodiac);
    border-bottom: 1px solid var(--blue-zodiac);
  }
`;

export default UserPageEditorPlugins;
