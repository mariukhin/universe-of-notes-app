import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import editFolderButton from 'Assets/docs/edit-folder-button.png';
import folderEditMode from 'Assets/docs/folder-edit-mode.png';
import folderSaved from 'Assets/docs/folder-saved.png';

import { ImageContainer, LiContent, Paragraph } from './styledComponents';

const UserPageFoldersListEdit = () => (
  <>
    <Paragraph>There are two ways to edit folder:</Paragraph>
    <UserPageFoldersListEdit.UL>
      <li>
        <UserPageFoldersListEdit.OL>
          <li>
            <LiContent>
              Click the right mouse button on the folder list to display a
              context menu.
            </LiContent>
            <ImageContainer width={240}>
              <LazyImage src={editFolderButton} alt="Edit Folder Button" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Press &#xab;Edit&#xbb; button.</LiContent>
          </li>
          <li>
            <LiContent>Enter the unique folder name in user context.</LiContent>
            <ImageContainer width={200}>
              <LazyImage src={folderEditMode} alt="Edit Folder Mode" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>
              Press Enter or change focus to save the folder.
            </LiContent>
            <ImageContainer width={200}>
              <LazyImage src={folderSaved} alt="Folder Saved" />
            </ImageContainer>
          </li>
        </UserPageFoldersListEdit.OL>
      </li>
      <li>
        <LiContent>
          Press enter key on the keyboard and repeat steps 3 & 4.
        </LiContent>
      </li>
    </UserPageFoldersListEdit.UL>
  </>
);

UserPageFoldersListEdit.OL = styled.ol`
  padding-left: 30px;
`;

UserPageFoldersListEdit.UL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export default UserPageFoldersListEdit;
