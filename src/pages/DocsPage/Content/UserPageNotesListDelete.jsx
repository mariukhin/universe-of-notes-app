import React from 'react';
import styled from 'styled-components';

import { DOCS_PAGES } from 'Pages/DocsPage/utils';

import { LazyImage } from 'Components';

import deleteNoteButton from 'Assets/docs/delete-note-button.png';

import {
  ContentLink,
  ImageContainer,
  LiContent,
  Paragraph,
} from './styledComponents';

const UserPageNotesListDelete = () => (
  <>
    <Paragraph>
      After deleting the note goes to the folder &#xab;Recently Deleted&#xbb;
      and is recoverable. There are three ways to delete note:
    </Paragraph>
    <UserPageNotesListDelete.UL>
      <li>
        <UserPageNotesListDelete.OL>
          <li>
            <LiContent>
              Click the right mouse button on the note list to display a context
              menu.
            </LiContent>
          </li>
          <li>
            <LiContent>Press &#xab;Delete&#xbb; button.</LiContent>
            <ImageContainer width={240}>
              <LazyImage src={deleteNoteButton} alt="Delete Note Button" />
            </ImageContainer>
          </li>
        </UserPageNotesListDelete.OL>
      </li>
      <li>
        <LiContent>Press delete key on the keyboard.</LiContent>
      </li>
      <li>
        <LiContent>
          Check{' '}
          <ContentLink to={`/docs/${DOCS_PAGES.USER_PAGE_ACTIONS}`}>
            User Actions
          </ContentLink>
          .
        </LiContent>
      </li>
    </UserPageNotesListDelete.UL>
  </>
);

UserPageNotesListDelete.OL = styled.ol`
  padding-left: 30px;
`;

UserPageNotesListDelete.UL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export default UserPageNotesListDelete;
