import React from 'react';
import styled from 'styled-components';

import { LazyImage } from 'Components';

import focusEditorImg from 'Assets/docs/focus-editor.png';
import selectedEditorTextImg from 'Assets/docs/selected-editor-text.png';
import toolbarImg from 'Assets/docs/toolbar.png';

import { otherActionsConfig, stylingActionsConfig } from '../utils';

import {
  DiskUL,
  ImageContainer,
  InlineImageContainer,
  Paragraph,
  ULli,
} from './styledComponents';

const UserPageEditorActions = () => (
  <>
    <Paragraph>
      The toolbar provides a wide range of options for working with the editor.
      All items have necessary tooltips.
    </Paragraph>
    <ImageContainer width={900}>
      <LazyImage src={toolbarImg} alt="Toolbar" />
    </ImageContainer>
    <Paragraph>
      To apply any styling action - select the desired area & then use the
      action. For formatting actions - just put focus on the required area of
      the editor.
    </Paragraph>
    <DiskUL>
      {stylingActionsConfig.map(item => (
        <ULli key={item.actionName}>
          <UserPageEditorActions.ActionName>
            {item.actionName}
          </UserPageEditorActions.ActionName>
          <div>
            <UserPageEditorActions.OL>
              <UserPageEditorActions.OLli>
                <UserPageEditorActions.ContentGrouper>
                  <UserPageEditorActions.ContentText>
                    {item.block ? 'Focus editor.' : 'Select text.'}
                  </UserPageEditorActions.ContentText>
                  <InlineImageContainer width={60}>
                    <LazyImage
                      src={item.block ? focusEditorImg : selectedEditorTextImg}
                      alt="Selected Editor Text"
                    />
                  </InlineImageContainer>
                </UserPageEditorActions.ContentGrouper>
              </UserPageEditorActions.OLli>
              <UserPageEditorActions.OLli>
                <UserPageEditorActions.ContentGrouper>
                  <UserPageEditorActions.ContentText>
                    Choose action.
                  </UserPageEditorActions.ContentText>
                  <InlineImageContainer
                    width={item.actionImgWidth ? item.actionImgWidth : 40}
                  >
                    <LazyImage
                      src={item.actionImg}
                      alt={`${item.actionName} Action`}
                    />
                  </InlineImageContainer>
                </UserPageEditorActions.ContentGrouper>
              </UserPageEditorActions.OLli>
              {item.additionalAction && (
                <UserPageEditorActions.OLli>
                  {item.additionalAction.img ? (
                    <UserPageEditorActions.ContentGrouper>
                      <UserPageEditorActions.ContentText>
                        {item.additionalAction.text}
                      </UserPageEditorActions.ContentText>
                      <InlineImageContainer
                        width={item.additionalAction.imgWidth}
                      >
                        <LazyImage
                          src={item.additionalAction.img}
                          alt={item.additionalAction.text}
                        />
                      </InlineImageContainer>
                    </UserPageEditorActions.ContentGrouper>
                  ) : (
                    item.additionalAction.text
                  )}
                </UserPageEditorActions.OLli>
              )}
              <UserPageEditorActions.OLli>
                <UserPageEditorActions.ContentGrouper>
                  <UserPageEditorActions.ContentText>
                    See result.
                  </UserPageEditorActions.ContentText>
                  <InlineImageContainer
                    width={item.resultImgWidth ? item.resultImgWidth : 60}
                  >
                    <LazyImage
                      src={item.resultImg}
                      alt={`${item.actionName} Result`}
                    />
                  </InlineImageContainer>
                </UserPageEditorActions.ContentGrouper>
              </UserPageEditorActions.OLli>
            </UserPageEditorActions.OL>
          </div>
        </ULli>
      ))}
    </DiskUL>
    <Paragraph>Other actions:</Paragraph>
    <DiskUL>
      {otherActionsConfig.map(item => (
        <ULli key={item.actionName}>
          <UserPageEditorActions.ContentGrouper style={{ marginBottom: 10 }}>
            <UserPageEditorActions.ActionName style={{ margin: '0 10px 0 0' }}>
              {item.actionName}
            </UserPageEditorActions.ActionName>
            <InlineImageContainer width={40}>
              <LazyImage
                src={item.actionImg}
                alt={`${item.actionName} Action`}
              />
            </InlineImageContainer>
          </UserPageEditorActions.ContentGrouper>
          <UserPageEditorActions.Description>
            {item.actionDescription}
          </UserPageEditorActions.Description>
        </ULli>
      ))}
    </DiskUL>
  </>
);

UserPageEditorActions.ActionName = styled.p`
  font-weight: 500;
  margin-bottom: 10px;
`;

UserPageEditorActions.ContentGrouper = styled.div`
  display: flex;
  align-items: center;
`;

UserPageEditorActions.ContentText = styled.p`
  margin-right: 10px;
`;

UserPageEditorActions.Description = styled.p``;

UserPageEditorActions.OL = styled.ol`
  display: flex;
  align-items: center;
  padding-left: 30px;
`;

UserPageEditorActions.OLli = styled.li`
  margin-right: 40px;
`;

export default UserPageEditorActions;
