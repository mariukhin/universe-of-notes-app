import React from 'react';

import { LazyImage } from 'Components';

import foldersListImg from 'Assets/docs/user-page-folders-list.png';

import { ImageContainer, Paragraph } from './styledComponents';

const UserPageFoldersList = () => (
  <>
    <Paragraph>
      Folder list provides user ability to manage folders. Available all CRUD
      operations including filter by folder name field.
    </Paragraph>
    <ImageContainer width={200}>
      <LazyImage src={foldersListImg} alt="Folders List" />
    </ImageContainer>
  </>
);

export default UserPageFoldersList;
