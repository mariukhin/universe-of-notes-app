import React from 'react';
import styled from 'styled-components';

import { DOCS_PAGES } from 'Pages/DocsPage/utils';

import { LazyImage } from 'Components';

import deleteFolderButton from 'Assets/docs/delete-folder-button.png';
import folderDeleteConfirmation from 'Assets/docs/folder-delete-confirmation.png';

import {
  ContentLink,
  ImageContainer,
  LiContent,
  Paragraph,
} from './styledComponents';

const UserPageFoldersListDelete = () => (
  <>
    <Paragraph>There are three ways to delete folder:</Paragraph>
    <UserPageFoldersListDelete.UL>
      <li>
        <UserPageFoldersListDelete.OL>
          <li>
            <LiContent>
              Click the right mouse button on the folder list to display a
              context menu.
            </LiContent>
            <ImageContainer width={240}>
              <LazyImage src={deleteFolderButton} alt="Delete Folder Button" />
            </ImageContainer>
          </li>
          <li>
            <LiContent>Press &#xab;Delete&#xbb; button.</LiContent>
          </li>
          <li>
            <LiContent>
              Confirm action. <u>Important</u>: notes in the current folder
              cannot be restored.
            </LiContent>
            <ImageContainer width={360}>
              <LazyImage
                src={folderDeleteConfirmation}
                alt="Folder Delete Confirmation"
              />
            </ImageContainer>
          </li>
        </UserPageFoldersListDelete.OL>
      </li>
      <li>
        <LiContent>
          Press delete key on the keyboard and confirm action.
        </LiContent>
      </li>
      <li>
        <LiContent>
          Check{' '}
          <ContentLink to={`/docs/${DOCS_PAGES.USER_PAGE_ACTIONS}`}>
            User Actions
          </ContentLink>
          .
        </LiContent>
      </li>
    </UserPageFoldersListDelete.UL>
  </>
);

UserPageFoldersListDelete.OL = styled.ol`
  padding-left: 30px;
`;

UserPageFoldersListDelete.UL = styled.ul`
  padding-left: 70px;
  list-style: disc;
`;

export default UserPageFoldersListDelete;
