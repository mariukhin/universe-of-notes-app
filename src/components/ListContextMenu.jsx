import PropTypes from 'prop-types';

import { Menu } from 'antd';
import {
  DeleteOutlined,
  EditOutlined,
  SettingOutlined,
} from '@ant-design/icons';

import React from 'react';
import styled from 'styled-components';

const ListContextMenu = ({
  createItem,
  deleteItem,
  editItem,
  type = 'folders',
}) => (
  <div>
    <ContextMenu>
      <ContextMenuItem key="1" onClick={editItem}>
        <span>
          <SettingOutlined />
          <span>Edit</span>
        </span>
      </ContextMenuItem>
      <ContextMenuItem key="2" onClick={deleteItem}>
        <span>
          <DeleteOutlined />
          <span>Delete</span>
        </span>
      </ContextMenuItem>
      {type === 'notes' && (
        <ContextMenuItem key="3" onClick={createItem}>
          <span>
            <EditOutlined />
            <span>Create</span>
          </span>
        </ContextMenuItem>
      )}
    </ContextMenu>
  </div>
);

ListContextMenu.propTypes = {
  createItem: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  editItem: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

const ContextMenu = styled(Menu)`
  width: 100px;
  border: none;

  & .ant-menu-item-active {
    background-color: var(--blue-zodiac) !important;
    color: var(--merino) !important;
  }
`;

const ContextMenuItem = styled(Menu.Item)`
  outline: 1px solid var(--soft-amber);
  margin: 0 !important;
  color: var(--scroll-bar);
  background-color: var(--white) !important;
`;

export default ListContextMenu;
