import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';

import { DOCS_PAGES } from 'Pages/DocsPage/utils';
import { routePaths } from 'Routes';

const Footer = () => {
  const location = useLocation();
  const isUserPage = location.pathname === routePaths.user;

  return (
    <Footer.Wrapper isUserPage={isUserPage}>
      {isUserPage && (
        <div style={{ display: 'flex' }}>
          <Footer.Link to={routePaths.main}>Home</Footer.Link>
          <Footer.Divider />
          <Footer.Link to={`/docs/${DOCS_PAGES.USER_PAGE}`}>Docs</Footer.Link>
        </div>
      )}
      <p>© 2020 Max Team. All rights reserved.</p>
    </Footer.Wrapper>
  );
};

Footer.Divider = styled.div`
  width: 1px;
  background-color: var(--hurricane);
  margin-right: 10px;
`;

Footer.Link = styled(Link)`
  line-height: 1;
  color: var(--blue-zodiac);

  &:first-child {
    margin-right: 10px;
  }

  &:hover {
    color: var(--blue-zodiac);
    border-bottom: 1px solid var(--blue-zodiac);
  }
`;

Footer.Wrapper = styled.footer`
  border-top: 1px solid var(--hurricane);
  padding: 5px 20px;
  display: flex;
  justify-content: ${({ isUserPage }) =>
    isUserPage ? 'space-between' : 'flex-end'};
  align-items: center;
`;

export default Footer;
