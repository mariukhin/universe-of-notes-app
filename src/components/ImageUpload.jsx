import { Button, Upload } from 'antd';
import { FileImageOutlined } from '@ant-design/icons';

import React from 'react';
import styled from 'styled-components';

const ImageUpload = props => (
  <Upload
    accept="image/*"
    beforeUpload={() => false}
    showUploadList={false}
    {...props}
  >
    <ImageUpload.Button size="small" icon={<FileImageOutlined />} />
  </Upload>
);

ImageUpload.Button = styled(Button)`
  color: var(--blue-zodiac);
  border: 1px solid var(--blue-zodiac);
  margin-right: 10px;
  background-color: inherit;
`;

export default ImageUpload;
