import PropTypes from 'prop-types';

import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getAuthStatus } from 'Selectors/session';

import { routePaths } from 'Routes';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const isAuthenticated = useSelector(getAuthStatus);

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={routePaths.sign} />
        )
      }
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
};

export default PrivateRoute;
