import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

import React from 'react';

const { confirm } = Modal;

const confirmationDialog = ({ content, onOk, onCancel }) =>
  confirm({
    title: 'Are you sure?',
    icon: <ExclamationCircleOutlined />,
    content,
    okButtonProps: {
      style: { backgroundColor: 'var(--blue-zodiac)', border: 'none' },
    },
    cancelButtonProps: {
      style: { color: 'var(--hurricane)', border: 'none' },
    },
    onOk() {
      if (onOk) onOk();
    },
    onCancel() {
      if (onCancel) onCancel();
    },
  });

export default confirmationDialog;
