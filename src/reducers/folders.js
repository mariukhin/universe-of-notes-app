import * as R from 'ramda';

import * as actions from 'Actions/folders';

import { ERROR, FETCHING_STATE, START, SUCCESS } from 'Services/reduxHelpers';

export const ROOT = 'folders';

const initialState = {
  isDisabled: false,
  selectedFolder: {
    id: '',
    name: '',
    readonly: false,
    userId: '',
    key: '',
    edit: false,
    deletable: false,
    deleteFolder: () => null,
  },
  folders: {
    data: [],
    state: null,
  },
};

const foldersReducer = (state = initialState, { type, data, response }) => {
  switch (type) {
    case actions.SELECT_FOLDER:
      return R.mergeDeepRight(state, {
        selectedFolder: data.selectedFolder,
      });
    case actions.RESET_FOLDER_DELETABLE_STATE:
      return R.mergeDeepRight(state, {
        selectedFolder: {
          ...state.selectedFolder,
          deletable: false,
        },
      });
    case actions.SET_FOLDER_DELETABLE_STATE:
      return R.mergeDeepRight(state, {
        selectedFolder: {
          ...state.selectedFolder,
          deletable: true,
        },
      });
    case actions.FETCH_FOLDERS[START]:
      return R.mergeDeepRight(state, {
        folders: {
          data: R.pathOr([], [ROOT, 'data'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_FOLDERS[SUCCESS]:
      return R.mergeDeepRight(state, {
        folders: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_FOLDERS[ERROR]:
      return R.mergeDeepRight(state, {
        folders: {
          data: [],
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.FILTER_FOLDERS[START]:
      return R.mergeDeepRight(state, {
        folders: {
          data: R.pathOr([], [ROOT, 'data'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FILTER_FOLDERS[SUCCESS]:
      return R.mergeDeepRight(state, {
        folders: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FILTER_FOLDERS[ERROR]:
      return R.mergeDeepRight(state, {
        folders: {
          data: [],
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.DISABLE_FOLDERS: {
      return R.mergeDeepRight(state, {
        isDisabled: true,
      });
    }
    case actions.ENABLE_FOLDERS: {
      return R.mergeDeepRight(state, {
        isDisabled: false,
      });
    }
    case actions.RESET_FOLDER_STATE: {
      return initialState;
    }
    default:
      return state;
  }
};

export default foldersReducer;
