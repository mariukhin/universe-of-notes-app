import * as R from 'ramda';

import * as actions from 'Actions/global';

export const ROOT = 'global';

const initialState = {
  isFoldersListCollapsed: false,
};

const globalReducer = (state = initialState, { type }) => {
  switch (type) {
    case actions.TOGGLE_FOLDERS_LIST:
      return R.mergeDeepRight(state, {
        isFoldersListCollapsed: !R.prop('isFoldersListCollapsed', state),
      });

    default:
      return state;
  }
};

export default globalReducer;
