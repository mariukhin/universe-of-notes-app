import * as R from 'ramda';

import * as actions from 'Actions/notes';

import { ERROR, FETCHING_STATE, START, SUCCESS } from 'Services/reduxHelpers';

export const ROOT = 'notes';

const initialState = {
  isDisabled: false,
  selectedNote: {
    id: '',
    name: '',
    locked: false,
    active: true,
    content: '',
    folderId: '',
    userId: '',
    createDt: null,
    updateDt: null,
    key: '',
    edit: false,
    deletable: false,
    createNote: () => null,
    deleteNote: () => null,
    notesMenuItems: () => null,
    lockNote: () => null,
    state: null,
  },
  notes: {
    data: [],
    state: null,
  },
  likes: {
    data: [],
    state: null,
  },
  userNotes: {
    data: [],
    count: 0,
    state: null,
  },
};

const notesReducer = (state = initialState, { type, data, response }) => {
  switch (type) {
    case actions.SELECT_NOTE:
      return R.mergeDeepRight(state, {
        selectedNote: data.selectedNote,
      });
    case actions.RESET_NOTE_DELETABLE_STATE:
      return R.mergeDeepRight(state, {
        selectedNote: {
          ...state.selectedNote,
          deletable: false,
        },
      });
    case actions.SET_NOTE_DELETABLE_STATE:
      return R.mergeDeepRight(state, {
        selectedNote: {
          ...state.selectedNote,
          deletable: true,
        },
      });
    case actions.DISABLE_NOTES: {
      return R.mergeDeepRight(state, {
        isDisabled: true,
      });
    }
    case actions.ENABLE_NOTES: {
      return R.mergeDeepRight(state, {
        isDisabled: false,
      });
    }
    case actions.RESET_NOTE_STATE: {
      return initialState;
    }
    case actions.FETCH_RECENTLY_DELETED_NOTES[START]:
    case actions.FETCH_NOTES[START]:
    case actions.FILTER_NOTES[START]:
    case actions.FETCH_POPULAR_NOTES[START]:
      return R.mergeDeepRight(state, {
        notes: {
          data: R.pathOr([], [ROOT, 'data'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_RECENTLY_DELETED_NOTES[SUCCESS]:
    case actions.FETCH_NOTES[SUCCESS]:
    case actions.FILTER_NOTES[SUCCESS]:
      return R.mergeDeepRight(state, {
        notes: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_POPULAR_NOTES[SUCCESS]:
      return R.mergeDeepRight(state, {
        notes: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_RECENTLY_DELETED_NOTES[ERROR]:
    case actions.FILTER_NOTES[ERROR]:
    case actions.FETCH_NOTES[ERROR]:
    case actions.FETCH_POPULAR_NOTES[ERROR]:
      return R.mergeDeepRight(state, {
        notes: {
          data: [],
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.CREATE_NOTE[SUCCESS]:
      return state.notes.data.length === 0
        ? R.mergeDeepRight(state, {
            notes: {
              data: state.notes.data,
              state: FETCHING_STATE.LOADED,
            },
          })
        : state;
    case actions.GET_NOTE[START]:
      return R.mergeDeepRight(state, {
        selectedNote: {
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.GET_NOTE[SUCCESS]:
      return R.mergeDeepRight(state, {
        selectedNote: {
          ...R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_LIKES[START]:
      return R.mergeDeepRight(state, {
        likes: {
          data: R.pathOr([], [ROOT, 'data'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_LIKES[SUCCESS]:
      return R.mergeDeepRight(state, {
        likes: {
          data: R.propOr([], 'data', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_LIKES[ERROR]:
      return R.mergeDeepRight(state, {
        likes: {
          data: [],
          state: FETCHING_STATE.ERROR,
        },
      });
    case actions.FETCH_USER_NOTES[START]:
      return R.mergeDeepRight(state, {
        userNotes: {
          data: R.pathOr([], ['userNotes', 'data'], state),
          count: R.pathOr(0, ['userNotes', 'count'], state),
          state: FETCHING_STATE.FETCHING,
        },
      });
    case actions.FETCH_USER_NOTES[SUCCESS]:
      return R.mergeDeepRight(state, {
        userNotes: {
          data: R.propOr([], 'data', response),
          count: R.propOr(0, 'count', response),
          state: FETCHING_STATE.LOADED,
        },
      });
    case actions.FETCH_USER_NOTES[ERROR]:
      return R.mergeDeepRight(state, {
        userNotes: {
          data: [],
          count: 0,
          state: FETCHING_STATE.ERROR,
        },
      });
    default:
      return state;
  }
};

export default notesReducer;
