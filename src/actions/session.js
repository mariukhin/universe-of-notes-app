import { takeEvery, takeLatest } from 'redux-saga/effects';

import * as api from 'Api/session';

import {
  createActionCreator,
  createRequestTypes,
  createSagaWorker,
  ERROR,
  START,
  SUCCESS,
} from 'Services/reduxHelpers';

const namespace = 'SESSION';

export const REGISTER = createRequestTypes(`${namespace}/REGISTER`);

export const register = createActionCreator({
  type: REGISTER[START],
  expect: ['username', 'email', 'password'],
});

const registerWorker = createSagaWorker({
  _apiCall: api.register,
  successActionType: REGISTER[SUCCESS],
  errorActionType: REGISTER[ERROR],
});

export const LOGIN = createRequestTypes(`${namespace}/LOGIN`);

export const login = createActionCreator({
  type: LOGIN[START],
  expect: ['username', 'password'],
});

const loginWorker = createSagaWorker({
  _apiCall: api.login,
  successActionType: LOGIN[SUCCESS],
  errorActionType: LOGIN[ERROR],
});

export const LOGOUT = createRequestTypes(`${namespace}/LOGOUT`);

export const logout = createActionCreator({
  type: LOGOUT[START],
});

const logoutWorker = createSagaWorker({
  _apiCall: api.logout,
  successActionType: LOGOUT[SUCCESS],
  errorActionType: LOGOUT[ERROR],
});

export const REFRESH_USER = createRequestTypes(`${namespace}/REFRESH_USER`);

export const refreshUser = createActionCreator({
  type: REFRESH_USER[START],
});

const refreshUserWorker = createSagaWorker({
  _apiCall: api.refreshUser,
  successActionType: REFRESH_USER[SUCCESS],
  errorActionType: REFRESH_USER[ERROR],
});

export const UPDATE_USER = createRequestTypes(`${namespace}/UPDATE_USER`);

export const updateUser = createActionCreator({
  type: UPDATE_USER[START],
});

const updateUserWorker = createSagaWorker({
  _apiCall: api.updateUser,
  successActionType: UPDATE_USER[SUCCESS],
  errorActionType: UPDATE_USER[ERROR],
});

export const FETCH_POPULAR_USERS = createRequestTypes(
  `${namespace}/FETCH_POPULAR_USERS`,
);

export const fetchPopularUsers = createActionCreator({
  type: FETCH_POPULAR_USERS[START],
});

const fetchPopularUsersWorker = createSagaWorker({
  _apiCall: api.fetchPopularUsers,
  successActionType: FETCH_POPULAR_USERS[SUCCESS],
  errorActionType: FETCH_POPULAR_USERS[ERROR],
});

export function* sessionWatcher() {
  yield takeEvery(REGISTER[START], registerWorker);
  yield takeEvery(LOGIN[START], loginWorker);
  yield takeEvery(LOGOUT[START], logoutWorker);
  yield takeEvery(REFRESH_USER[START], refreshUserWorker);
  yield takeEvery(UPDATE_USER[START], updateUserWorker);
  yield takeLatest(FETCH_POPULAR_USERS[START], fetchPopularUsersWorker);
}
