import * as R from 'ramda';

import { ROOT } from 'Reducers/global';

export const getFoldersListToggledState = state =>
  R.path([[ROOT], 'isFoldersListCollapsed'], state);
