import * as R from 'ramda';

import { ROOT } from 'Reducers/folders';

export const getSelectedFolder = state =>
  R.path([[ROOT], 'selectedFolder'], state);

export const getFolders = state => R.path([[ROOT], 'folders'], state);

export const isFoldersDisabled = state => R.path([[ROOT], 'isDisabled'], state);
