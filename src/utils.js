import { notification } from 'antd';

import { convertFromRaw, EditorState } from 'draft-js';

import jstz from 'jstz';
import moment from 'moment-timezone';
import * as R from 'ramda';

export const isDev = process.env.NODE_ENV === 'development';

export const SIGN_FORMS = Object.freeze({
  REGISTER_FORM: 'REGISTER_FORM',
  SIGN_IN_FORM: 'SIGN_IN_FORM',
});

const isMenuItemName = (items, newMenuItemName) =>
  items.some(item => item.name === newMenuItemName);

export const getNewMenuItemName = (items, name = 'Folder') => {
  let newMenuItemName = `New ${name}`;
  let count = 1;
  while (isMenuItemName(items, newMenuItemName)) {
    newMenuItemName = `New ${name} ${count}`;
    count += 1;
  }

  return newMenuItemName;
};

export const getMenuItemsMaxKey = menuItems =>
  Math.max(...menuItems.map(item => item.key));

export const getItemByKey = (items, key) =>
  items.find(item => item.key === key);

export const getMenuItemById = (items, id) =>
  items.find(item => item.id === id);

export const getMenuItemId = (items, key) =>
  items.find(item => item.key === key).id;

export const getMenuItems = items =>
  items
    .map((item, idx) => ({ ...item, key: String(idx + 1), edit: false }))
    .reverse();

export const getSimpleMenuItems = items =>
  items.map((item, idx) => ({ ...item, key: String(idx + 1) }));

export const applyBlur = e => e?.currentTarget?.blur();

export const toBase64 = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

export const openNotificationWithIcon = ({ type, message, description }) =>
  notification[type]({
    message,
    description,
  });

export const NOTIFICATION_TYPES = Object.freeze({
  INFO: 'info',
  SUCCESS: 'success',
  WARNING: 'warning',
  ERROR: 'error',
});

export const getNoteTime = item => {
  const date = item.updateDt ? item.updateDt : item.createDt;
  const timezoneName = jstz.determine().name();
  return moment(date).tz(timezoneName).format('HH:mm');
};

export const getNoteDate = (item, isNotePage = false) => {
  const date = item.updateDt ? item.updateDt : item.createDt;
  const timezoneName = jstz.determine().name();
  const baseDate = moment(date).tz(timezoneName);
  const monthNumber = baseDate.month();
  const monthName = moment.months(monthNumber);
  const monthDay = baseDate.date();
  const year = baseDate.year();
  const time = baseDate.format('HH:mm');

  if (isNotePage) {
    return `${monthName} ${monthDay}, ${year} at ${time}`;
  }
  return `${
    item.updateDt ? 'Updated' : 'Created'
  }: ${monthName} ${monthDay}, ${year} at ${time}`;
};

export const getNotePreviewText = item => {
  try {
    const content = JSON.parse(item.content);
    const text = R.pathOr('', ['blocks', '0', 'text'], content);
    return text.trim().slice(0, 16);
  } catch {
    return item.content.length > 0 ? item.content : '';
  }
};

export const ACTIVE_PAGES = Object.freeze({
  MAIN_PAGE: 'MAIN_PAGE',
  USER_PAGE: 'USER_PAGE',
  USER_DETAILS_PAGE: 'USER_DETAILS_PAGE',
  NOTE_DETAILS_PAGE: 'NOTE_DETAILS_PAGE',
});

const PAGES_WITH_MAIN_HEADER = [
  ACTIVE_PAGES.MAIN_PAGE,
  ACTIVE_PAGES.USER_DETAILS_PAGE,
  ACTIVE_PAGES.NOTE_DETAILS_PAGE,
];

export const isMainHeader = activePage =>
  PAGES_WITH_MAIN_HEADER.includes(activePage);

export const USER_DETAILED_VIEW_MODES = Object.freeze({
  COLUMN_MODE: 'COLUMN_MODE',
  ROW_MODE: 'ROW_MODE',
});

export const getParseErrorContent = error =>
  `{"blocks":[{"key":"drpos","text":"${error} ","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}],"entityMap":{}}`;

export const isNoteEmpty = note => {
  if (note?.content) {
    try {
      const editorState = EditorState.createWithContent(
        convertFromRaw(JSON.parse(note.content)),
      );
      if (editorState.getCurrentContent().hasText()) {
        return false;
      }
    } catch (error) {
      EditorState.createWithContent(
        convertFromRaw(JSON.parse(getParseErrorContent(error))),
      );
      return false;
    }
  }
  return true;
};
