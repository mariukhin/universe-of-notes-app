import * as R from 'ramda';

import * as actions from 'Actions/session';

import { clearAuthHeader, setAuthHeader } from 'Services/axiosDefaults';
import { ERROR, START, SUCCESS } from 'Services/reduxHelpers';

const startAndSuccessRefresh = [
  actions.REFRESH_USER[START],
  actions.REFRESH_USER[SUCCESS],
];

const successRegisterAndLogin = [
  actions.REGISTER[SUCCESS],
  actions.LOGIN[SUCCESS],
];

const successLogoutAndRefreshError = [
  actions.LOGOUT[SUCCESS],
  actions.REFRESH_USER[ERROR],
];

const authHeaderMiddleware = ({ getState }) => next => action => {
  if (startAndSuccessRefresh.includes(action.type)) {
    const state = getState();
    const { token } = state.session;

    if (!token) return;

    setAuthHeader(token);
  }
  if (successRegisterAndLogin.includes(action.type)) {
    const token = R.pathOr('', ['response', 'data', 'token'])(action);

    setAuthHeader(token);
  }
  if (successLogoutAndRefreshError.includes(action.type)) {
    clearAuthHeader();
  }
  next(action);
};

export default authHeaderMiddleware;
