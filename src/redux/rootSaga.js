import { all } from 'redux-saga/effects';

import { foldersWatcher } from 'Actions/folders';
import { notesWatcher } from 'Actions/notes';
import { sessionWatcher } from 'Actions/session';

function* rootSaga() {
  yield all([sessionWatcher(), foldersWatcher(), notesWatcher()]);
}

export default rootSaga;
