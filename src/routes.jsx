import React, { lazy } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import { getAuthStatus, getRefreshStatus } from 'Selectors/session';

import { PageLoader, PrivateRoute } from 'Components';

const AppPage = lazy(() => import('Pages/AppPage'));

const DocsPage = lazy(() => import('Pages/DocsPage'));

const MainPage = lazy(() => import('Pages/MainPage'));

const NoteDetailsPage = lazy(() => import('Pages/NoteDetailsPage'));

const SignPage = lazy(() => import('Pages/SignPage'));

const UserDetailsPage = lazy(() => import('Pages/UserDetailsPage'));

const UserPage = lazy(() => import('Pages/UserPage'));

export const routePaths = {
  app: '/app',
  main: '/app/main',
  user: '/app/user',
  userDetails: '/app/user-details/:username',
  noteDetails: '/app/note-details/:noteId',
  docs: '/docs/:page',
  sign: '/sign',
};

const getRoutes = () => {
  const isRefreshed = useSelector(getRefreshStatus);
  const authStatus = useSelector(getAuthStatus);

  return isRefreshed ? (
    <Switch>
      <Route
        exact
        path={routePaths.sign}
        render={props =>
          !authStatus ? (
            <SignPage {...props} />
          ) : (
            <Redirect to={routePaths.main} />
          )
        }
      />
      <PrivateRoute
        path={routePaths.app}
        component={() => (
          <AppPage>
            <Switch>
              <Route exact path={routePaths.main} component={MainPage} />
              <Route exact path={routePaths.user} component={UserPage} />
              <Route
                exact
                path={routePaths.userDetails}
                component={UserDetailsPage}
              />
              <Route
                exact
                path={routePaths.noteDetails}
                component={NoteDetailsPage}
              />
              <Redirect to={routePaths.main} />
            </Switch>
          </AppPage>
        )}
      />
      <PrivateRoute exact path={routePaths.docs} component={DocsPage} />
      {authStatus ? (
        <Redirect to={routePaths.main} />
      ) : (
        <Redirect to={routePaths.sign} />
      )}
    </Switch>
  ) : (
    <PageLoader />
  );
};

export default getRoutes;
