import axios from 'axios';

const ROOT = '/folders';

export const fetchFolders = async () => {
  const url = ROOT;

  const { data } = await axios.get(url);

  return data;
};

export const createFolder = async ({ payload }) => {
  const url = ROOT;

  const { data } = await axios.post(url, payload);

  return data;
};

export const editFolder = async ({ payload, folderId }) => {
  const url = `${ROOT}/${folderId}`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const deleteFolder = async ({ folderId }) => {
  const url = `${ROOT}/${folderId}`;

  const { data } = await axios.delete(url);

  return data;
};

export const filterFolders = async ({ name }) => {
  const url = `${ROOT}/filter?name=${name}`;

  const { data } = await axios.get(url);

  return data;
};

export const fetchFolder = async ({ folderId }) => {
  const url = `${ROOT}/${folderId}`;

  const { data } = await axios.get(url);

  return data;
};
