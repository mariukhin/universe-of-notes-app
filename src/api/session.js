import axios from 'axios';

const ROOT = '/users';

export const register = async ({ payload }) => {
  const url = `${ROOT}/register`;

  const { data } = await axios.post(url, payload);

  return data;
};

export const login = async ({ payload }) => {
  const url = `${ROOT}/login`;

  const { data } = await axios.post(url, payload);

  return data;
};

export const logout = async () => {
  const url = `${ROOT}/logout`;

  const { data } = await axios.post(url);

  return data;
};

export const refreshUser = async () => {
  const url = `${ROOT}/current`;

  const { data } = await axios.get(url);

  return data;
};

export const updateUser = async ({ payload }) => {
  const url = `${ROOT}/update`;

  const { data } = await axios.put(url, payload);

  return data;
};

export const fetchPopularUsers = async () => {
  const url = `${ROOT}/popular?ordering=-rating&page=1`;

  const { data } = await axios.get(url);

  return data;
};
