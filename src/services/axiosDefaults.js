import axios from 'axios';

import { isDev } from 'Utils';

const devUrl = 'http://127.0.0.1:8000';
const prodUrl = 'https://api-universe-of-notes.herokuapp.com';

export const getBaseUrl = () => (isDev ? devUrl : prodUrl);

axios.defaults.baseURL = getBaseUrl();

export const setAuthHeader = token => {
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export const clearAuthHeader = () => {
  axios.defaults.headers.common.Authorization = null;
};
