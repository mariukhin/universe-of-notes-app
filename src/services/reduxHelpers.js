import * as R from 'ramda';
import { call, put } from 'redux-saga/effects';

export const START = 'START';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';

export const FETCHING_STATE = {
  FETCHING: 'FETCHING',
  LOADED: 'LOADED',
  ERROR: 'ERROR',
};

export function createRequestTypes(base) {
  return [START, SUCCESS, ERROR].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}

export const createActionCreator = ({ type, expect }) => data => {
  let propNames = [];
  if (data?.payload) {
    const { payload, ...rest } = data;
    propNames = Object.keys({ ...payload, ...rest });
  } else if (data) {
    propNames = Object.keys(data);
  }

  if (expect && !expect.every(key => propNames.includes(key))) {
    throw new Error('Props are invalid!');
  }

  return {
    type,
    ...data,
  };
};

export const createSagaWorker = ({
  _apiCall,
  successActionType,
  errorActionType,
}) =>
  function* sagaWorker({ onError, onSuccess, ...params }) {
    try {
      const response = yield call(_apiCall, params);
      yield put({ type: successActionType, response });
      if (onSuccess) {
        onSuccess(response);
      }
    } catch (error) {
      yield put({ type: errorActionType, error });
      if (onError) {
        const responseData = {
          data: R.pathOr('', ['response', 'data'])(error),
          status: R.pathOr('', ['response', 'status'])(error),
          statusText: R.pathOr('', ['response', 'statusText'])(error),
        };
        onError(responseData);
      }
    }
  };
